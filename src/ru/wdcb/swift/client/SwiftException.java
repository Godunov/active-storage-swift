package ru.wdcb.swift.client;

public class SwiftException extends Exception {

	private static final long serialVersionUID = 1L;

	public SwiftException(String s) {
		super(s);
	}
	
	public SwiftException(Throwable e) {
		super(e);
	}

	public SwiftException(String s, Throwable e) {
		super(s,e);
	}
	
}
