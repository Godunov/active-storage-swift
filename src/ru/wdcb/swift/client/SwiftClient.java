package ru.wdcb.swift.client;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.Response;

import org.openstack.api.storage.AccountResource;
import org.openstack.api.storage.ContainerResource;
import org.openstack.api.storage.ObjectResource;
import org.openstack.client.OpenStackClient;
import org.openstack.model.storage.StorageContainer;
import org.openstack.model.storage.swift.SwiftStorageObject;
import org.openstack.model.storage.swift.SwiftStorageObjectProperties;

public class SwiftClient {
	private Properties p = new Properties(); 
	
	public OpenStackClient openstack;
	
	public AccountResource storage; 
	
    public SwiftClient() throws SwiftException {
		try {
			p.load(new FileInputStream("config.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		p.getProperty("identity.endpoint.publicURL");
		p.getProperty("auth.credentials");
		p.getProperty("auth.username");
		p.getProperty("auth.password");
		p.getProperty("auth.tenantName");
		
		openstack = OpenStackClient.authenticate(p); // Keystone authentication
		storage = openstack.getStorageEndpoint();    // Access to Swift Storage - endpoint 
	}
    
    /**
     * Get the list of all storage containers
     * @return List<StorageContainer> 
     */
    public List<StorageContainer> getContainersList() {
    	return storage.get();
    }
    
    /**
     * Get the container's resource 
     * @param container
     * @return ContainerResource
     */
    public ContainerResource getContainer(String container) {
    	return storage.container(container);
    }
    
    /**
     * Get the list of container's objects
     * @param container
     * @return List<SwiftStorageObject>
     */
    public List<SwiftStorageObject> getObjectsList(String container) {
    	return storage.container(container).get();
    }
    
    /**
     * Create new container in the storage
     * @param container
     */
    public void createContainer(String container) {
    	Response response = storage.container(container).put();
    }
    
    /**
     * Delete container from storage
     * @param container
     * @throws SwiftException
     */
    public void deleteContainer(String container) throws SwiftException {
    	if (storage.container(container).get().size() > 0) {
    		deleteAllObjects(container);
    	}
    	storage.container(container).delete();
    }
    
    /**
     * Get the object from container 
     * @param container
     * @param object
     * @return ObjectResource
     */
    public ObjectResource getObject(String container, String object) {
    	return storage.container(container).object(object);
    }
    
    /**
     * Get object's data from container 
     * @param container
     * @param object
     * @return InputStream
     */
    public java.io.InputStream getData(String container, String object) {
    	return storage.container(container).object(object).openStream();
    }
    
    /**
     * Put object into container
     * @param container
     * @param object
     * @param data
     */
    public void putObject(String container, String object, byte[] data) {
    	InputStream is = new ByteArrayInputStream(data); 
    	SwiftStorageObjectProperties properties = new SwiftStorageObjectProperties();
		Response response = storage.container(container).object(object).put(is, data.length, properties);
    }
    
    /**
     * Delete object from container
     * @param container
     * @param object
     */
    public void deleteObject(String container, String object) {
    	storage.container(container).object(object).delete();
    }
    
    /**
     * Delete all objects from non-empty container
     * @param container
     * @throws SwiftException
     */
    public void deleteAllObjects(String container) throws SwiftException {
    	List<SwiftStorageObject> objects = storage.container(container).get();
    	try {
			Iterator<SwiftStorageObject> it = objects.iterator();
			while (it.hasNext()) {
				String object = it.next().getName();
				deleteObject(container, object);
			}
		}
		catch (Exception e) {
			throw new SwiftException("Could not delete items",e);
		}
    }
    
    /**
     * Delete all containers from storage
     * @throws SwiftException
     */
	public void deleteAll() throws SwiftException {
		List<StorageContainer> containers = getContainersList();
		try {
			Iterator<StorageContainer> it = containers.iterator();
			while (it.hasNext()) {
				String container = it.next().getName();
				deleteContainer(container);
			}
		}
		catch (SwiftException e) {
			throw new SwiftException("Could not delete container",e);
		}
	}
}
