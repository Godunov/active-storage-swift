package ru.wdcb.activestorage;

import java.sql.*;
import java.util.*;


/**
 * An ActiveStorage group.
 * @author  dmedv
 */
public class Group {
	
	/**
	 * @uml.property  name="database"
	 * @uml.associationEnd  
	 */
	protected Database database;
	protected int id;
	
	/**
	 * @uml.property  name="name"
	 */
	private String name;

	protected Group(Database database, int id) throws ActiveStorageException { 
		this.database = database;
		this.id = id;
		
		String commandText = 
			"SELECT group_name FROM groups WHERE group_id = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			ResultSet reader = command.executeQuery();
			reader.next();
			name = reader.getString(1);
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not initialize group", e);
		}
	}
	
	/**
	 * Returns the database this group belongs to.
	 * @return  database
	 * @uml.property  name="database"
	 */
	public Database getDatabase() {
		return database;
	}
	
	
	/**
	 * Deletes all variables of the group from the database.
	 * @throws ActiveStorageException 
	 */
	public void deleteGroupVariables() throws ActiveStorageException {
		List<Variable> variables = this.listVariables();
		Iterator<Variable> vi = variables.iterator();
		while(vi.hasNext()) {
			Variable v = vi.next();
			v.delete();
		}
	}
	
	/**
	 * Deletes all dimensions of the group from the database.
	 * @throws ActiveStorageException 
	 */
	public void deleteGroupDimensions() throws ActiveStorageException {
		List<Dimension> dimensions = this.listDimensions();
		Iterator<Dimension> di = dimensions.iterator();
		while(di.hasNext()) {
			Dimension d = di.next();
			d.delete();
		}
	}
	
	/**
	 * Deletes this group from the database.
	 * @throws ActiveStorageException
	 */
	public void delete() throws ActiveStorageException {
		deleteGroupVariables();
		deleteGroupDimensions();
		String commandText = 
			"DELETE FROM groups WHERE group_id = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			command.execute();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not delete group", e);
		}
	}
	
	/**
	 * Creates a new sub-group with the given name.
	 * @param groupName group name
	 * @return group
	 * @throws ActiveStorageException
	 */
	public Group createSubgroup(String groupName) throws ActiveStorageException {
		if (getSubgroup(groupName) != null) return getSubgroup(groupName); 
		String commandText = 
    		"INSERT groups(group_name,parent_id) VALUES(?,?)";
        try {
            PreparedStatement command = database.connection.prepareStatement(commandText);
	        command.setString(1, groupName);
	        command.setInt(2, id);
	        command.executeUpdate();
	        int groupId = (int)((com.mysql.jdbc.PreparedStatement)command).getLastInsertID();
			command.close();
			
	        return new Group(database,groupId);
        }
        catch (Exception e) {
        	throw new ActiveStorageException("Could not create sub-group", e);
        }
	}
	
	/**
	 * Returns a list of all sub-groups of this group.
	 * @return list of groups
	 * @throws ActiveStorageException
	 */
	public List<Group> listSubgroups() throws ActiveStorageException {
		List<Group> groups = new ArrayList<Group>();
		Map<Integer,String> map = getParentGroups(id);
		Iterator<Integer> it = map.keySet().iterator();
		while (it.hasNext()) {
			int i = it.next();
			groups.add(new Group(database,i));
		}
		return groups;
	}
	
	/**
	 * Returns the name for this group.
	 * @return  group name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns a dimension with the given name.
	 * @param dimName dimension name
	 * @return dimension
	 * @throws ActiveStorageException
	 */
	public Dimension getDimension(String dimName) throws ActiveStorageException {
		Map<Integer,String> map = getGroupDims(id);
		Iterator<Integer> it = map.keySet().iterator();
		while (it.hasNext()) {
			int key = it.next();
			if (map.get(key).equals(dimName)) {
				if (isAmbiguousDimension(dimName)) {
					throw new ActiveStorageException("Dimension is ambiguous");
				}
				else {
					return new Dimension(this,key);
				}
			}
		}
		throw new ActiveStorageException("Dimension not found");
	}
	
	/**
	 * Returns a list of dimensions for this group.
	 * @return list of dimensions
	 * @throws ActiveStorageException
	 */
	public List<Dimension> listDimensions() throws ActiveStorageException {
		List<Dimension> list = new ArrayList<Dimension>();
		Map<Integer, String> map = getGroupDims(id);
		Iterator<Integer> it = map.keySet().iterator();
		while (it.hasNext()) {
			list.add(new Dimension(this,it.next()));
		}
		return list;
	}
	
	/**
	 * Returns a list of variables for this group.
	 * @return list of variables
	 * @throws ActiveStorageException
	 */
	public List<Variable> listVariables() throws ActiveStorageException {
		
		List<Variable> list = new ArrayList<Variable>();
		String commandText = 
			"SELECT var_id FROM variables WHERE var_group = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			while (reader.next()) {
				int varId = reader.getInt(1);
				list.add(new Variable(this,varId));	
			}
			reader.close();
			command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get a list of variables", e);
		}
		return list;
	}
	
	/**
	 * Returns a variable with the given name.
	 * @param variableName variable name
	 * @return variable
	 * @throws ActiveStorageException
	 */
	public Variable getVariable(String variableName) throws ActiveStorageException {
		String commandText = 
			"SELECT var_id FROM variables WHERE var_group = ? AND var_name = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			command.setString(2, variableName);
			ResultSet reader = command.executeQuery();
			if (reader.next()) { 
				int varId = reader.getInt(1);
				return new Variable(this,varId);
			}
			else {
				throw new ActiveStorageException("Variable not found");
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get variable", e);
		}
	}
	
	/**
	 * Returns a list of attributes for this group.
	 * @return list of attributes
	 * @throws ActiveStorageException
	 */
	public List<Attribute> listAttributes() throws ActiveStorageException {
		List<Attribute> list = new ArrayList<Attribute>();		
		String commandText =
			"SELECT att_name, type_name, att_value FROM grp_attributes JOIN types ON att_type = type_id WHERE group_id = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			while (reader.next()) {
				String record = reader.getString(3);
				String typeName = reader.getString(2);
				String name = reader.getString(1);
				list.add(Attribute.parse(name, record, ru.wdcb.activestorage.DataType.getCodeForName(typeName)));
			}
			reader.close();
			command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get a list of attributes", e);
		}
		return list;
	}
	
	/**
	 * Creates a new dimension with the given name.
	 * @param dimName dimension name
	 * @return dimension
	 * @throws ActiveStorageException
	 */
	public Dimension createDimension(String dimName) throws ActiveStorageException {
		
		String commandText = 
                "INSERT dimensions(dim_group, dim_name, dim_length)"+
                " VALUES (?,?,0)";
        try {
        	List<Dimension> list = listDimensions();
        	Iterator<Dimension> i = list.iterator();
        	while (i.hasNext()) {
        		Dimension dim = i.next();
        		if (dim.getName().equals(dimName)) {
        			throw new ActiveStorageException ("Dimension already exists");
        		}
        	}

        	PreparedStatement command = database.connection.prepareStatement(commandText);
	        command.setInt(1, this.id);
	        command.setString(2, dimName);

	        command.executeUpdate();
	        int dimId = (int)((com.mysql.jdbc.PreparedStatement)command).getLastInsertID();
	        command.close();
			return new Dimension(this,dimId);
        }
        catch (Exception e) {
        	throw new ActiveStorageException("Could not create dimension", e);
        }
	}
	    
	/**
	 * Creates a new variable with the specified name, dimensions and data type.
	 * @param varName variable name
	 * @param type data type
	 * @param dimensions array of dimensions
	 * @return variable
	 * @throws ActiveStorageException
	 */
	public Variable createVariable(String varName, ru.wdcb.activestorage.DataType type, Dimension[] dimensions) throws ActiveStorageException {
        try {
        	database.connection.setAutoCommit(false);
        	for (int i=0; i<dimensions.length; i++) {
    			if (isAmbiguousDimension(dimensions[i].getName())) {
    				throw new ActiveStorageException("Dimension is ambiguous");
    			}
    		}
    		

           
            String insertVariables = 
                    "INSERT variables(var_group,var_name,var_type,data_table,index_table,vector_size)" +
                    " SELECT ?,?,type_id,?,?,1 FROM types WHERE type_name = ?";

    		PreparedStatement command2 = database.connection.prepareStatement(insertVariables);
	        command2.setInt(1, id);
	        command2.setString(2, varName);
	        command2.setString(3, "");
	        command2.setString(4, "");
	        command2.setString(5, type.getName());
	        command2.executeUpdate();
	        
	        int varId = (int)((com.mysql.jdbc.PreparedStatement)command2).getLastInsertID();
	        command2.close();
	        
        	String indexTableName = varName.replaceAll(" ", "_")+"_"+varId+"_index";
        	String dataTableName = varName.replaceAll(" ", "_")+"_"+varId+"_data";

        	Statement command = database.connection.createStatement();
       	 
            String createDirectory =
                    "CREATE TABLE " + indexTableName + "(";
                	createDirectory +=
                        "chunk_key int NOT NULL," +
                        "dim_index int NOT NULL," +
                        "key_min int NOT NULL," +
                        "key_max int NOT NULL," +
                        "CONSTRAINT PK_" + indexTableName + " PRIMARY KEY (" +
                            "chunk_key ASC," +
                            "dim_index ASC" +
                        ")" +
                    ")";
            command.execute(createDirectory);
            
            String createIndex =
                    "CREATE INDEX IX_" + indexTableName + 
                    " ON " + indexTableName + " " +
                        "("+
                        	"dim_index," +
                            "key_max ASC," +
                            "key_min ASC" +
                        ")";
            command.execute(createIndex);
            command.close();
            
            String updateTableNames = 
            		"UPDATE variables SET data_table=?, index_table=? WHERE var_id=?";
            command2 = database.connection.prepareStatement(updateTableNames);
            command2.setString(1, dataTableName);
            command2.setString(2, indexTableName);
            command2.setInt(3, varId);
            command2.executeUpdate();
            command2.close();
            
            String insertShapes = 
	        		"INSERT shapes(var_id,dim_index,dim_id,dim_type) VALUES(?,?,?,0)";
	        command2 = database.connection.prepareStatement(insertShapes);
            for (int i = 0; i < dimensions.length; i++)
            {
                command2.setInt(1, varId);
                command2.setInt(2, i);
                command2.setInt(3, dimensions[i].id);
                command2.executeUpdate();
            }
  
            database.swiftClient.createContainer(dataTableName);
            
            database.connection.commit();
            
			return new Variable(this,varId);
        }
        catch (Exception e) {
        	try {
        		database.connection.rollback();
        	}
        	catch (Exception f) {}
        	throw new ActiveStorageException("Could not create variable", e);
        }
        finally {
        	try {
        		database.connection.setAutoCommit(true);
        	}
        	catch (Exception e) {};
        }
	}
	
	public Variable createVariable(String varName, ucar.ma2.DataType type, Dimension[] dimensions) throws ActiveStorageException {
        try {
        	database.connection.setAutoCommit(false);
        	for (int i=0; i<dimensions.length; i++) {
    			if (isAmbiguousDimension(dimensions[i].getName())) {
    				throw new ActiveStorageException("Dimension is ambiguous");
    			}
    		}
    		

           
            String insertVariables = 
                    "INSERT variables(var_group,var_name,var_type,data_table,index_table,vector_size)" +
                    " SELECT ?,?,type_id,?,?,1 FROM types WHERE type_name = ?";

    		PreparedStatement command2 = database.connection.prepareStatement(insertVariables);
	        command2.setInt(1, id);
	        command2.setString(2, varName);
	        command2.setString(3, "");
	        command2.setString(4, "");
	        command2.setString(5, type.name());
	        command2.executeUpdate();
	        
	        int varId = (int)((com.mysql.jdbc.PreparedStatement)command2).getLastInsertID();
	        command2.close();
	        
        	String indexTableName = varName.replaceAll(" ", "_")+"_"+varId+"_index";
        	String dataTableName = varName.replaceAll(" ", "_")+"_"+varId+"_data";

        	Statement command = database.connection.createStatement();
       	 
            String createDirectory =
                    "CREATE TABLE " + indexTableName + "(";
                	createDirectory +=
                        "chunk_key int NOT NULL," +
                        "dim_index int NOT NULL," +
                        "key_min int NOT NULL," +
                        "key_max int NOT NULL," +
                        "CONSTRAINT PK_" + indexTableName + " PRIMARY KEY (" +
                            "chunk_key ASC," +
                            "dim_index ASC" +
                        ")" +
                    ")";
            command.execute(createDirectory);
            
            String createIndex =
                    "CREATE INDEX IX_" + indexTableName + 
                    " ON " + indexTableName + " " +
                        "("+
                        	"dim_index," +
                            "key_max ASC," +
                            "key_min ASC" +
                        ")";
            command.execute(createIndex);
            command.close();
            
            String updateTableNames = 
            		"UPDATE variables SET data_table=?, index_table=? WHERE var_id=?";
            command2 = database.connection.prepareStatement(updateTableNames);
            command2.setString(1, dataTableName);
            command2.setString(2, indexTableName);
            command2.setInt(3, varId);
            command2.executeUpdate();
            command2.close();
            
            String insertShapes = 
	        		"INSERT shapes(var_id,dim_index,dim_id,dim_type) VALUES(?,?,?,0)";
	        command2 = database.connection.prepareStatement(insertShapes);
            for (int i = 0; i < dimensions.length; i++)
            {
                command2.setInt(1, varId);
                command2.setInt(2, i);
                command2.setInt(3, dimensions[i].id);
                command2.executeUpdate();
            }
  
            database.swiftClient.createContainer(dataTableName);
            
            database.connection.commit();
            
			return new Variable(this,varId);
        }
        catch (Exception e) {
        	try {
        		database.connection.rollback();
        	}
        	catch (Exception f) {}
        	throw new ActiveStorageException("Could not create variable", e);
        }
        finally {
        	try {
        		database.connection.setAutoCommit(true);
        	}
        	catch (Exception e) {};
        }
	}
	
	private boolean isAmbiguousDimension(String dimName) throws ActiveStorageException {
		List<Dimension> list = listDimensions();

		int count = 0;
		Iterator<Dimension> i = list.iterator();
		while (i.hasNext()) {
			Dimension dim = i.next();
			if (dim.getName().equals(dimName)) {
				count++;
			}
		}
		if (count > 1) {
			return true;
		}
		else { 
			return false; 
		}
	}
	
	/** 
	 * Returns a group attribute with the given name.
	 * @param attributeName attribute name
	 * @return attribute
	 * @throws ActiveStorageException
	 */
	public Attribute getAttribute(String attributeName) throws ActiveStorageException {
		String commandText =
			"SELECT type_name, att_value FROM grp_attributes JOIN types ON att_type = type_id WHERE group_id = ? AND att_name = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			command.setString(2, attributeName);
			ResultSet reader = command.executeQuery();
			if (reader.next()) {
				String typeName = reader.getString(1);
				String record = reader.getString(2);
				return Attribute.parse(attributeName, record, ru.wdcb.activestorage.DataType.getCodeForName(typeName));
			}	
			else {
				return null;
				//throw new ActiveStorageException("Attribute does not exist");
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get attribute", e);
		}
	}
	
	/**
	 * Sets the given group attribute.
	 * @param attribute attribute
	 * @throws ActiveStorageException
	 */
	public void setAttribute(Attribute attribute) throws ActiveStorageException {
		String commandText =
                "REPLACE grp_attributes(group_id,att_name,att_type,att_value)" +
                " SELECT ?,?,type_id,? FROM types WHERE type_name=?";

		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			command.setString(2, attribute.getName());
			command.setString(3, attribute.getString());
			command.setString(4, attribute.getDataType().getName());
			command.executeUpdate();
			command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not set attribute", e);
		}
	}

	/**
	 * Deletes a group attribute with the given name.
	 * @param attributeName attribute name
	 * @throws ActiveStorageException
	 */
	public void deleteAttribute(String attributeName) throws ActiveStorageException {
		String commandText =
                "DELETE FROM grp_attributes" +
                " WHERE att_name = ? AND group_id = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setString(1, attributeName);
			command.setInt(2, this.id);
			command.execute();
			command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not delete attribute", e);
		}
	}
	
	/**
	 * Returns a sub-group under this group with the given name
	 * @param groupName sub-group name
	 * @return <code>Group</code> object
	 * @throws ActiveStorageException
	 */
	public Group getSubgroup(String groupName) throws ActiveStorageException {
		Group result = null;
		String commandText = 
			"SELECT group_id FROM groups WHERE parent_id = ? AND group_name = ?";
		
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			command.setString(2, groupName);
			ResultSet reader = command.executeQuery();
			if (reader.next()) {
				result =  new Group(database,reader.getInt(1));
			}
			return result;
		}	
		catch (Exception e) {
			throw new ActiveStorageException("Could not get subgroup");
		}
	}
	
	
	
	
	protected Map<Integer,String> getParentGroups(int groupId) throws ActiveStorageException {
		Map<Integer,String> result = new Hashtable<Integer,String>();
		
		String commandText1 = 
				"SELECT group_id, group_name FROM groups WHERE group_id = ?";
		String commandText2 = 
				"SELECT parent_id FROM groups WHERE group_id = ?";
		try {
			int currentId = groupId;
			while (currentId != -1) {
				PreparedStatement command1 = database.connection.prepareStatement(commandText1);
				command1.setInt(1, currentId);
				ResultSet reader1 = command1.executeQuery();
				while (reader1.next()) {
					result.put(reader1.getInt(1),reader1.getString(2));
				}
				reader1.close();
				command1.close();
				
				PreparedStatement command2 = database.connection.prepareStatement(commandText2);
				command2.setInt(1, currentId);
				ResultSet reader2 = command2.executeQuery();
				if (reader2.next()) {
					currentId = reader2.getInt(1);
				}
				else {
					currentId = -1;
				}
				reader2.close();
				command2.close();
			}
			
			return result;
		}	
		catch (Exception e) {
			throw new ActiveStorageException("Could not get parent groups",e);
		}
	}
	
	protected Map<Integer,String> getGroupDims(int groupId) throws ActiveStorageException {
		Map<Integer,String> result = new Hashtable<Integer,String>();
		String commandText = 
				"SELECT dim_id, dim_name FROM dimensions WHERE dim_group = ?";
		try {
			PreparedStatement command = database.connection.prepareStatement(commandText);
			Map<Integer,String> map = getParentGroups(groupId);
			Iterator<Integer> it = map.keySet().iterator();
			while (it.hasNext()) {
				command.setInt(1,it.next());
				ResultSet reader = command.executeQuery();
				while (reader.next()) {
					result.put(reader.getInt(1),reader.getString(2));
				}
				reader.close();
			}
			command.close();
			
			return result;
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get group dimensions",e);
		}
	}

}
