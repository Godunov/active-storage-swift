package ru.wdcb.activestorage.loader;

public enum NCEPFileNamesEnum {
	AIR("air"), AIR_SIG995("air.sig995");
	
	private NCEPFileNamesEnum(final String fname) {
        this.fname = fname;
    }

    private final String fname;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return fname;
    }
}
