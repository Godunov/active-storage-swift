package ru.wdcb.activestorage.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import ru.wdcb.activestorage.ActiveStorageException;
import ru.wdcb.activestorage.DataType;
import ru.wdcb.activestorage.Database;
import ru.wdcb.activestorage.Dimension;
import ru.wdcb.activestorage.Group;
//import ru.wdcb.activestorage.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class DataLoader extends NCEPFileType {
	
	public static String fileName;
	public static NCEPLevelsEnum level;
	public static NCEPVariablesEnum variable;
	public static File ncepFile = new File("D:/NetCDF/air.sig995.2012.nc");
	public static NetcdfFile netCDFFile;
	public static List<Dimension[]> dimensionsDB;
	public static List<ucar.nc2.Variable> variablesNC2;
	public static List<ucar.nc2.Dimension> dimensionsNC2;
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ActiveStorageException {
		
		Properties p = new Properties();
		p.load(new FileInputStream("config.properties"));
		String connectionString = p.getProperty("database.url");
		Database db = new Database(connectionString);

		setFileParameters();
		
		// Create new group or set existing group for current file type
		Group group = db.getRootGroup().createSubgroup(String.valueOf(level).toLowerCase());
		
		try {
			netCDFFile = NetcdfFile.open(ncepFile.getPath());
		} catch (Exception e) {
			System.out.println("Couldn't open file");
		}
	
		// Get the variables for current NetCDF file
		variablesNC2 = netCDFFile.getVariables();
		dimensionsNC2 = netCDFFile.getDimensions();
		System.out.println(dimensionsNC2);
		
		// Create dimensions in the database
		for (Iterator<ucar.nc2.Dimension> dim = dimensionsNC2.iterator(); dim.hasNext();  ) {
			ucar.nc2.Dimension item = dim.next();
			try {
				group.getDimension(item.getName());
			} catch (Exception e) {
				group.createDimension(item.getName());
			}
		}
		
		// Create variables for database
		for (Iterator<Variable> var = variablesNC2.iterator(); var.hasNext();  ) {
			List<ucar.nc2.Dimension> dim = var.next().getDimensions();
			Variable varItem = var.next();
			Dimension[] dimDB = new Dimension[dim.size()];
			for (int j=0;j<dim.size();j++) dimDB[j] = group.getDimension(dim.get(j).getName()); 
			try { 
				group.getVariable(varItem.getFullName());
			} catch (Exception e) {
				group.createVariable(varItem.getFullName(), varItem.getDataType(), dimDB);	
			}
		}
		
	}
	
	/**
	 * Gets the file name from the path
	 * @return
	 */
	private static String getFileName() {
		String path = ncepFile.getPath();
		return path.substring(path.lastIndexOf(File.separatorChar)+1, path.length() - 8);
	}
	
	/**
	 * Search file parameters (level and variable)
	 * @param fileName
	 */
	public static void setFileParameters() {
		fileName = getFileName();
		for (String key : NCEPFileType.ftype.keySet()) {
			if (fileName.equals(key)) {
				ComplexHashValue attributes = NCEPFileType.ftype.get(key);
				level = attributes.getLevel();
				variable = attributes.getVariable();				
			} 
		}
	}
	
	
}
