package ru.wdcb.activestorage.loader;

public enum NCEPVariablesEnum {
	AIR_TEMPERATURE,
	PRESSURE,
	SURFACE_LIFTED_INDEX,
	SEA_LEVEL_PRESSURE;
}
