package ru.wdcb.activestorage.loader;

/**
 * Interface for NCEP Loader for different types of files
 * @author Grigorieva_MA
 *
 */
public interface IDataLoader {
	
	public NCEPFileType getFileType();
	
	
	
}
