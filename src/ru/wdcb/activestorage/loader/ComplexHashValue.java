package ru.wdcb.activestorage.loader;

import org.openstack.model.compute.nova.server.actions.GetVncConsoleAction;

public class ComplexHashValue {
	
	protected NCEPLevelsEnum level;
	protected NCEPVariablesEnum variable;
	
	public ComplexHashValue(NCEPLevelsEnum level, NCEPVariablesEnum variable) {
		this.level = level;
		this.variable = variable;
	}
	
	public NCEPLevelsEnum getLevel() {
		return level;
	}
	
	public NCEPVariablesEnum getVariable() {
		return variable;
	}
		
}
