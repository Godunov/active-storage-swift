package ru.wdcb.activestorage.loader;

import java.util.HashMap;

public class NCEPFileType {

	public static HashMap<String, ComplexHashValue> ftype = 
			new HashMap<String, ComplexHashValue>();
	
	public NCEPFileType() {
		ftype.put("air", new ComplexHashValue(NCEPLevelsEnum.PRESSURE, NCEPVariablesEnum.AIR_TEMPERATURE));
		ftype.put("air.sig995", new ComplexHashValue(NCEPLevelsEnum.SURFACE, NCEPVariablesEnum.AIR_TEMPERATURE));
	}
		
	
}
