package ru.wdcb.activestorage.loader;

public enum NCEPLevelsEnum {
	PRESSURE, SURFACE, SURFACE_FLUX, OTHER_FLUX, TROPOPAUSE, SPECTRAL_COEFF;
}
