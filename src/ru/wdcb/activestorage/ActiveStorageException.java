package ru.wdcb.activestorage;

public class ActiveStorageException extends Exception {

	private static final long serialVersionUID = 1L;

	public ActiveStorageException(String s) {
		super(s);
	}
	
	public ActiveStorageException(Throwable e) {
		super(e);
	}

	public ActiveStorageException(String s, Throwable e) {
		super(s,e);
	}
	
}
