package ru.wdcb.activestorage;

/**
 * An ActiveStorage attribute.
 * @author  dmedv
 */
public class Attribute {
	/**
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="values"
	 */
	private Object values;
	/**
	 * @uml.property  name="type"
	 * @uml.associationEnd  
	 */
	private DataType type;
	
	/**
	 * Creates a new attribute from a <code>char</code> array.
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, char[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.CHAR, values.length);
	}
	
	/**
	 * Creates a new attribute from a <code>byte</code> array.
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, byte[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.BYTE, values.length);		
	}

	/**
	 * Creates a new attribute from a <code>short</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, short[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.SHORT, values.length);		
	}

	/**
	 * Creates a new attribute from a <code>int</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, int[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.INT, values.length);		
	}
	
	/**
	 * Creates a new attribute from a <code>long</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, long[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.LONG, values.length);		
	}

	/**
	 * Creates a new attribute from a <code>float</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, float[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.FLOAT, values.length);		
	}

	/**
	 * Creates a new attribute from a <code>double</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, double[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.DOUBLE, values.length);		
	}

	/**
	 * Creates a new attribute from a <code>String</code> array. 
	 * @param name attribute name
	 * @param values array of values
	 * @throws ActiveStorageException
	 */
	public Attribute(String name, String[] values) throws ActiveStorageException {
		this.name = name;
		this.values = values;
		this.type = new DataType(DataType.STRING);
	}
	
	/**
	 * Returns attribute contents as string.
	 * @return attribute contents
	 * @throws ActiveStorageException
	 */
	public String getString() throws ActiveStorageException {
		String s = "";
		switch (type.getTypeCode()) {
			case DataType.CHAR: { 
				char[] v = (char[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.BYTE: { 
				byte[] v = (byte[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.SHORT: { 
				short[] v = (short[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.INT: { 
				int[] v = (int[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.LONG: { 
				long[] v = (long[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.FLOAT: { 
				float[] v = (float[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.DOUBLE: { 
				double[] v = (double[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += " ";
				}
				return s;
			}
			case DataType.STRING: {
				String[] v = (String[])values;
				for (int i=0; i<v.length; i++) {
					s += v[i];
					if (i!=(v.length-1)) s += '\n';
				}
				return s;			
			}
			default:
				throw new ActiveStorageException("Illegal type code");
		}
	}
	
	/**
	 * Parses attribute contents from string. 
	 * @param attributeName attribute name
	 * @param record attribute contents as string
	 * @param typeCode type code
	 * @return <code>Attribute</code> object
	 * @throws ActiveStorageException
	 */
	protected static Attribute parse(String attributeName, String record, int typeCode) throws ActiveStorageException {
		String[] s = record.split(" ");
		int n = s.length;
		switch (typeCode) {
			case DataType.CHAR: {
				char[] values  = new char[n];
				for (int i=0; i<n; i++) values[i] = s[i].charAt(0);
				return new Attribute(attributeName,values);
			}
			case DataType.BYTE: {
				byte[] values  = new byte[n];
				for (int i=0; i<n; i++) values[i] = Byte.parseByte(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.SHORT: {
				short[] values  = new short[n];
				for (int i=0; i<n; i++) values[i] = Short.parseShort(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.INT: {
				int[] values  = new int[n];
				for (int i=0; i<n; i++) values[i] = Integer.parseInt(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.LONG: {
				long[] values  = new long[n];
				for (int i=0; i<n; i++) values[i] = Long.parseLong(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.FLOAT: {
				float[] values  = new float[n];
				for (int i=0; i<n; i++) values[i] = Float.parseFloat(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.DOUBLE: {
				double[] values  = new double[n];
				for (int i=0; i<n; i++) values[i] = Double.parseDouble(s[i]);
				return new Attribute(attributeName,values);
			}
			case DataType.STRING: {
				s = record.split("\n");
				n = s.length;
				String[] values = new String[n];
				for (int i=0; i<n; i++) values[i] = s[i];
				return new Attribute(attributeName,values);
			}
			default:
				throw new ActiveStorageException("Illegal type code");	
		}
	}
	
	/**
	 * Returns the name for this attribute.
	 * @return  attribute name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the data type for this attribute.
	 * @return <code>DataType</code> object
	 */
	public DataType getDataType() {
		return type;
	}
	
	/**
	 * Returns attribute values.
	 * @return  array of values
	 * @uml.property  name="values"
	 */
	public Object getValues() {
		return values;
	}
}
