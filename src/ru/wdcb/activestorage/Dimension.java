package ru.wdcb.activestorage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * An ActiveStorage dimension.
 * @author  dmedv
 */
public class Dimension {

	/**
	 * @uml.property  name="group"
	 * @uml.associationEnd  
	 */
	protected Group group;
	protected int id;
	/**
	 * @uml.property  name="name"
	 */
	private String name;
	
	/**
	 * Constructs a <code>Dimension</code> with the given group and dimension id.
	 * @param group the group this dimension belongs to
	 * @param id dimension id
	 * @throws ActiveStorageException
	 */
	protected Dimension(Group group, int id) throws ActiveStorageException {
		this.group = group;
		this.id = id;
		
		
		String commandText = 
			"SELECT dim_name FROM dimensions WHERE dim_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			ResultSet reader = command.executeQuery();
			reader.next();
			name = reader.getString(1);
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not initialize dimension",e);
		}
	}
	
	/**
	 * Returns the group this dimension belongs to as a <code>Group</code> object.
	 * @return  group
	 * @uml.property  name="group"
	 */
	public Group getGroup() {
		return group;
	}
	
	/**
	 * Returns the name for this dimension.
	 * @return  dimension name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Deletes this variable from the database.
	 * @throws ActiveStorageException
	 */
	public void delete() throws ActiveStorageException {
		String commandText = 
                "DELETE FROM dimensions"+
                " WHERE dim_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			command.execute();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not delete dimension",e);
		}
	}
	
	/**
	 * Returns the current length for this dimension.
	 * @return dimension length
	 * @throws ActiveStorageException
	 */
	public int getLength() throws ActiveStorageException {
		String commandText = 
			"SELECT dim_length FROM dimensions WHERE dim_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			reader.next();
			return reader.getInt(1);
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get dimension length",e);
		}
	}
	
	/**
	 * Sets the new length for this dimension.
	 * @return dimension length
	 * @throws ActiveStorageException
	 */
	protected void setLength(int length) throws ActiveStorageException {
		String commandText = 
			"UPDATE dimensions SET dim_length = ? WHERE dim_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, length);
			command.setInt(2, this.id);
			command.execute();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get dimension length",e);
		}
	}
}
