package ru.wdcb.activestorage;

/**
 * A data type for variables and attributes.
 * @author  dmedv
 */
public class DataType {
	
	public static final int CHAR = 1;
	public static final int BYTE = 2;
	public static final int SHORT = 3;
	public static final int INT = 4;
	public static final int LONG = 5;
	public static final int FLOAT = 6;
	public static final int DOUBLE = 7;
	public static final int STRING = 8;
	
	/**
	 * @uml.property  name="length"
	 */
	private int length;
	/**
	 * @uml.property  name="typeCode"
	 */
	private int typeCode;
	
	/**
	 * Creates a new data type with the default length 1.
	 * @param typeCode type code
	 * @throws ActiveStorageException
	 */
	public DataType (int typeCode) throws ActiveStorageException {
		this(typeCode, 1);
	}
	
	/**
	 * Creates a new data type with specified type code and length.
	 * @param typeCode type code
	 * @param length type length (number of elements)
	 * @throws ActiveStorageException
	 */
	public DataType (int typeCode, int length) throws ActiveStorageException {
		if (typeCode>8 || typeCode<1) {
			throw new ActiveStorageException("Illegal type code");
		}
		if (length <= 0) {
			throw new ActiveStorageException("Type length must be greater than zero");
		}
		this.typeCode = typeCode;
		this.length = length;
	}
	
	/**
	 * Returns the name for this data type.
	 * @return type name
	 * @throws ActiveStorageException
	 */
	public String getName() throws ActiveStorageException {
		switch (typeCode) {
			case CHAR: 
				return "char";
			case BYTE: 
				return "byte";
			case SHORT: 
				return "short";
			case INT: 
				return "int";
			case LONG: 
				return "long";
			case FLOAT: 
				return "float";
			case DOUBLE: 
				return "double";
			case STRING: 
				return "String";
			default:
				throw new ActiveStorageException("Illegal type code");
		}
	}
	
	/**
	 * Returns the type code for this type.
	 * @return  type code
	 * @uml.property  name="typeCode"
	 */
	public int getTypeCode() {
		return typeCode;
	}
	
	/**
	 * Returns a type code for the given type name.
	 * @param typeName
	 * @return type code
	 * @throws ActiveStorageException
	 */
	public static int getCodeForName(String typeName) throws ActiveStorageException {

		if ("char".equals(typeName)) return CHAR;
		else if ("byte".equals(typeName)) return BYTE;
		else if ("short".equals(typeName)) return SHORT;
		else if ("int".equals(typeName)) return INT;
		else if ("long".equals(typeName)) return LONG;
		else if ("float".equals(typeName)) return FLOAT;
		else if ("double".equals(typeName)) return DOUBLE;
		else if ("String".equals(typeName)) return STRING;	
		else {
			throw new ActiveStorageException("Illegal type name");
		}
	}
	
	/**
	 * Returns the type length for this data type.
	 * @return  type length
	 * @uml.property  name="length"
	 */
	public int getLength() {
		return length;
	}
	
	/**
	 * Returns the total size for this data type. <code>total_size = type_length * primitive_size</code>
	 * @return total size
	 * @throws ActiveStorageException
	 */
	public int getTotalSize() throws ActiveStorageException {
		return length * getPrimitiveSize();
	}
	
	/**
	 * Returns the primitive size for this data type (the size of a single element).
	 * @return primitive size
	 * @throws ActiveStorageException
	 */
	public int getPrimitiveSize() throws ActiveStorageException {
		switch (typeCode) {
			case BYTE: 
			case CHAR:	
				return 1;
			case SHORT:
				return 2;
			case FLOAT:
			case INT: 
				return 4;
			case LONG: 
			case DOUBLE:
				return 8;
			default: 
				throw new ActiveStorageException("Illegal type code");	
		}
	}
	
	/** 
	 * Returns the size of the given array of this data type.
	 * @param shape array shape
	 * @return array size
	 * @throws ActiveStorageException
	 */
	public int getArraySize(int[] shape) throws ActiveStorageException {
		int result = 1;
		
		for (int i=0; i<shape.length; i++) {
			result *= shape[i];
		}
		
		return result * getTotalSize();
	}
}
