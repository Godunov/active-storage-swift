package ru.wdcb.activestorage;

import java.io.*;
import java.nio.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openstack.api.storage.ContainerResource;
import org.openstack.api.storage.ObjectResource;
import org.openstack.model.storage.StorageContainer;

import ru.wdcb.swift.client.SwiftException;

/**
 * An ActiveStorage variable.
 * @author  dmedv
 */
public class Variable {
	
	private static Log log = LogFactory.getLog(Variable.class);

	/**
	 * @uml.property  name="group"
	 * @uml.associationEnd  
	 */
	protected Group group;
	protected int id;
	/**
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="type"
	 * @uml.associationEnd  
	 */
	private DataType type;
	/**
	 * @uml.property  name="byteOrder"
	 */
	private ByteOrder byteOrder;
	
	/**
	 * Constructs a <code>Variable</code> with the given group and variable id and the default byte order (big-endian).
	 * @param group the group this variable belongs to
	 * @param id variable id
	 * @throws ActiveStorageException
	 */
	protected Variable(Group group, int id) throws ActiveStorageException {
		this(group, id, ByteOrder.BIG_ENDIAN);
	}	
	
	/**
	 * Constructs a <code>Variable</code> with the given group, variable id and byte order.
	 * @param the group this variable belongs to
	 * @param id
	 * @param byteOrder
	 * @throws ActiveStorageException
	 */
	protected Variable(Group group, int id, ByteOrder byteOrder) throws ActiveStorageException {
		this.group = group;
		this.id = id;
		
		String commandText = 
			"SELECT var_name, vector_size, type_name FROM variables JOIN types ON var_type = type_id WHERE var_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			ResultSet reader = command.executeQuery();
			reader.next();
			this.name = reader.getString(1);
			this.type = new DataType(DataType.getCodeForName(reader.getString(3)),reader.getInt(2));
			this.byteOrder = byteOrder;
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not initialize variable",e);
		}
	}
	
	/**
	 * Sets the byte order for this variable.
	 * @param byteOrder  byte order
	 * @uml.property  name="byteOrder"
	 */
	public void setByteOrder(ByteOrder byteOrder) {
		this.byteOrder = byteOrder;
	}

	/**
	 * Gets the byte order for this variable.
	 * @return  byte order
	 * @uml.property  name="byteOrder"
	 */
	public ByteOrder getByteOrder() {
		return byteOrder;
	}
	
	/**
	 * Returns the group this variable belongs to as a <code>Group</code> object.
	 * @return  group group
	 * @uml.property  name="group"
	 */
	public Group getGroup() {
		return group;
	}
	
	/**
	 * Returns the name for this variable.
	 * @return  variable name
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Deletes this variable from the database.
	 * @throws ActiveStorageException
	 */
	public void delete() throws ActiveStorageException {

		try {
			String commandText = 
					"SELECT data_table, index_table FROM variables WHERE var_id = ?";
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, id);
			ResultSet reader = command.executeQuery();
			reader.next();
			String dataTableName = reader.getString(1);
			String indexTableName = reader.getString(2);
			reader.close();
			command.close();
			
			try {
				group.database.connection.setAutoCommit(false);
				
				commandText = "DELETE FROM variables WHERE var_id = ?";
				command = group.database.connection.prepareStatement(commandText);
				command.setInt(1, id);
				command.executeUpdate();
				command.close();
				//group.database.swiftClient.deleteAll();
				List<StorageContainer> containers = group.database.swiftClient.getContainersList();
				try {
					Iterator<StorageContainer> it = containers.iterator();
					while (it.hasNext()) {
						group.database.swiftClient.deleteContainer(it.next().getName());
					}
				}
				catch (Exception e) {
					throw new SwiftException("Could not delete items",e);
				}

				group.database.swiftClient.deleteContainer(dataTableName);
				
				Statement st = group.database.connection.createStatement();
				st.executeUpdate("DROP TABLE " + indexTableName);
				st.close();
				
				group.database.connection.commit();
			}
			catch (Exception e) {
        		group.database.connection.rollback();
        		throw e;
        	}
			finally {
        		group.database.connection.setAutoCommit(true);
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not delete variable",e);
		}
	}
	
	/**
	 * Returns a set of attributes for this variable as a <code>List</code> of <code>Attribute</code> objects in no particular order.
	 * @return list of attributes
	 * @throws ActiveStorageException
	 */
	public List<Attribute> listAttributes() throws ActiveStorageException {
		List<Attribute> list = new ArrayList<Attribute>();
		String commandText =
			"SELECT att_name, type_name, att_value FROM var_attributes JOIN types ON att_type = type_id WHERE var_id = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			while (reader.next()) {
				String record = reader.getString(3);
				String typeName = reader.getString(2);
				String name = reader.getString(1);
				list.add(Attribute.parse(name, record, DataType.getCodeForName(typeName)));
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get a list of attributes",e);
		}
		return list;
	}
	
	/**
	 * Returns a set of dimensions for this variable as a <code>List</code> of <code>Dimension</code> objects.
	 * @return list of dimension
	 * @throws ActiveStorageException
	 */
	public List<Dimension> listDimensions() throws ActiveStorageException {
		List<Dimension> list = new ArrayList<Dimension>();
		String commandText = 
			"SELECT dim_id FROM (SELECT shapes.dim_id,dim_index FROM shapes " +
			"JOIN variables ON shapes.var_id = variables.var_id " +
			"JOIN dimensions ON shapes.dim_id = dimensions.dim_id " +
			"WHERE variables.var_id=?) a ORDER BY dim_index";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			while (reader.next()) {
				list.add(new Dimension(group,reader.getInt(1)));
			}
			return list;
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not a get a list of dimensions",e);
		}
	}
	
	/**
	 * Returns the data type for this variable as a <code>DataType</code> object.
	 * @return data type for the variable
	 */
	public DataType getDataType() {
		return type;
	}
	
	/**
	 * Returns the whole data array for this variable. The type of the returned array is determined by the variable data type. 
	 * @return data array
	 * @throws ActiveStorageException
	 */
	public Object getData() throws ActiveStorageException {
	
		int[] size = getShape();
		int[] origin = new int[size.length];
		Arrays.fill(origin, 0);
		return getData(origin, size);
	}
	
	/**
	 * Writes the given data array to the database at the given location. The type of the array is determined by the variable data type.
	 * @param origin data location
	 * @param shape data shape
	 * @param data data
	 * @throws ActiveStorageException
	 */
	public void putData(int[] origin, int[] shape, Object data) throws ActiveStorageException {
		switch (type.getTypeCode()) {
			case DataType.CHAR: putDataChar(origin, shape, (char[])data); break;
			case DataType.BYTE: putDataByte(origin, shape, (byte[])data); break;
			case DataType.SHORT: putDataShort(origin, shape, (short[])data); break;
			case DataType.INT: putDataInt(origin, shape, (int[])data); break;
			case DataType.LONG: putDataLong(origin, shape, (long[])data); break;
			case DataType.FLOAT: putDataFloat(origin, shape, (float[])data); break;
			case DataType.DOUBLE: putDataDouble(origin, shape, (double[])data); break;
			default: 			
				throw new ActiveStorageException("Illegal type code");		
		}
	}
	
	private void putDataChar(int[] origin, int[] shape, char[] data) throws ActiveStorageException { }
	
	private void putDataByte(int[] origin, int[] shape, byte[] data) throws ActiveStorageException { 
		write(origin,shape,data);
	}
	
	private void putDataShort(int[] origin, int[] shape, short[] data) throws ActiveStorageException {         
		
			byte[] buffer = new byte[data.length * type.getPrimitiveSize()];
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	        byteBuffer.order(byteOrder);
	        
			for (int i=0; i < data.length; i++) {
	        	byteBuffer.putShort(data[i]);
	        }
	        write(origin,shape,buffer);
	}
	
	private void putDataInt(int[] origin, int[] shape, int[] data) throws ActiveStorageException { 
			byte[] buffer = new byte[data.length * type.getPrimitiveSize()];
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	        byteBuffer.order(byteOrder);
	        
			for (int i=0; i < data.length; i++) {
	        	byteBuffer.putInt(data[i]);
	        }
	        
	        write(origin,shape,buffer);
	}
	
	private void putDataLong(int[] origin, int[] shape, long[] data) throws ActiveStorageException { 
			byte[] buffer = new byte[data.length * type.getPrimitiveSize()];
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	        byteBuffer.order(byteOrder);
	        
			for (int i=0; i < data.length; i++) {
	        	byteBuffer.putLong(data[i]);
	        }
	        
	        write(origin,shape,buffer);
	}
	
	private void putDataFloat(int[] origin, int[] shape, float[] data) throws ActiveStorageException { 
			byte[] buffer = new byte[data.length * type.getPrimitiveSize()];
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	        byteBuffer.order(byteOrder);
	        
			for (int i=0; i < data.length; i++) {
	        	byteBuffer.putFloat(data[i]);
	        }
	        
	        write(origin,shape,buffer);
	}
	
	private void putDataDouble(int[] origin, int[] shape, double[] data) throws ActiveStorageException { 
			byte[] buffer = new byte[data.length * type.getPrimitiveSize()];
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	        byteBuffer.order(byteOrder);
	        
			for (int i=0; i < data.length; i++) {
	        	byteBuffer.putDouble(data[i]);
	        }
	        
	        write(origin,shape,buffer);
	}
	
	private void write(int[] origin, int[] shape, byte[] chunk) throws ActiveStorageException {
    	int dimensionCount = origin.length;
    	int dataOffset = dimensionCount * 8;
    	
    	byte[] buffer = new byte[dataOffset + chunk.length];
    	
        for (int i = 0; i < dimensionCount; i++)
        {
        	byte[] headerMin = intToByteArray(origin[i]);
        	byte[] headerMax = intToByteArray(origin[i]+shape[i]-1);
            System.arraycopy(headerMin, 0, buffer, i * 8, 4);
            System.arraycopy(headerMax, 0, buffer, i * 8 + 4, 4);
        }
    	System.arraycopy(chunk, 0, buffer, dataOffset, chunk.length);
   	
    	try {
        	String sql = 
        			"SELECT index_table, data_table FROM variables WHERE var_id = ?";
        	
        	PreparedStatement command = group.database.connection.prepareStatement(sql);
        	command.setInt(1,this.id);
        	ResultSet rs = command.executeQuery();
        	String dataTableName, indexTableName;
        	if (rs.next()) {
        		indexTableName = rs.getString(1);
        		dataTableName = rs.getString(2);
        	}
        	else {
        		throw new ActiveStorageException("Variable not found");
        	}
        	rs.close();
        	command.close();
        	
    		group.database.connection.setAutoCommit(false);
    		
    		sql = "SELECT IFNULL(MAX(chunk_key)+1,0) FROM "+indexTableName;
    		Statement st = group.database.connection.createStatement();
    		rs = st.executeQuery(sql);
    		rs.next();
    		int chunkKey = rs.getInt(1);
    		rs.close();
    		st.close();
    		
    		sql = "INSERT "+indexTableName+"(chunk_key,dim_index,key_min,key_max) VALUES(?,?,?,?)";
    		command = group.database.connection.prepareStatement(sql);
    		
    		for (int i=0; i<dimensionCount; i++) {
    			command.setInt(1, chunkKey);
    			command.setInt(2, i);
    			command.setInt(3, origin[i]);
    			command.setInt(4, origin[i]+shape[i]-1);
    			command.executeUpdate();
    		}
    		
    		group.database.swiftClient.putObject(dataTableName, "data_"+chunkKey, buffer);
    		group.database.connection.commit();
    	}
    	catch (Exception e) {
        	try {
        		group.database.connection.rollback();
        	}
        	catch (Exception f) {}
        	throw new ActiveStorageException("Could not write data", e);
    	}
    	finally {
        	try {
        		group.database.connection.setAutoCommit(true);
        	}
        	catch (Exception e) {};
    	}
	}
	
	/**
	 * Returns the specified data sub-array. The type of the returned array is determined by the variable data type.
	 * @param origin data location
	 * @param shape data shape
	 * @return data array
	 * @throws ActiveStorageException
	 */
	public Object getData(int[] origin, int[] shape) throws ActiveStorageException {
		switch (type.getTypeCode()) {
			case DataType.CHAR: return getDataChar(origin, shape);
			case DataType.BYTE: return getDataByte(origin, shape);
			case DataType.SHORT: return getDataShort(origin, shape);
			case DataType.INT: return getDataInt(origin, shape);
			case DataType.LONG: return getDataLong(origin, shape);
			case DataType.FLOAT: return getDataFloat(origin, shape);
			case DataType.DOUBLE: return getDataDouble(origin, shape);
			default: 			
				throw new ActiveStorageException("Illegal type code");
		}
	}
	
	private char[] getDataChar(int[] origin, int[] shape) {
		char[] result = null;
		return result;
	}

	private byte[] getDataByte(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] result = new byte[type.getArraySize(shape)];
		gather(result,origin,shape);
		return result;
	}

	private short[] getDataShort(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] buffer = new byte[type.getArraySize(shape)];
		gather(buffer,origin,shape);
		
		short[] result = new short[buffer.length / type.getPrimitiveSize()];
		
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(byteOrder);
        
        for (int i=0; i<result.length; i++) {
        	result[i] = byteBuffer.getShort();      	
        }
		
        return result;
	}

	private int[] getDataInt(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] buffer = new byte[type.getArraySize(shape)];
		gather(buffer,origin,shape);
		
		int[] result = new int[buffer.length / type.getPrimitiveSize()];
		
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(byteOrder);
        
        for (int i=0; i<result.length; i++) {
        	result[i] = byteBuffer.getInt();      	
        }
		
        return result;
	}

	private long[] getDataLong(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] buffer = new byte[type.getArraySize(shape)];
		gather(buffer,origin,shape);
		
		long[] result = new long[buffer.length / type.getPrimitiveSize()];
		
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(byteOrder);
        
        for (int i=0; i<result.length; i++) {
        	result[i] = byteBuffer.getLong();      	
        }
		
        return result;
	}

	private float[] getDataFloat(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] buffer = new byte[type.getArraySize(shape)];
		gather(buffer,origin,shape);
		
		float[] result = new float[buffer.length / type.getPrimitiveSize()];
		
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(byteOrder);
        
        for (int i=0; i<result.length; i++) {
        	result[i] = byteBuffer.getFloat();
        }
		
        return result;
	}

	private double[] getDataDouble(int[] origin, int[] shape) throws ActiveStorageException {
		byte[] buffer = new byte[type.getArraySize(shape)];
		gather(buffer,origin,shape);
		
		double[] result = new double[buffer.length / type.getPrimitiveSize()];
		
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(byteOrder);
        
        for (int i=0; i<result.length; i++) {
        	result[i] = byteBuffer.getDouble();      	
        }
		
        return result;
	}
	
	/**
	 * Returns the shape of a particular array in collection.
	 * @param position array position
	 * @return array shape
	 * @throws ActiveStorageException
	 */
	public int[] getShape(int[] position) throws ActiveStorageException {
		int[] result = null;
		ArrayList<Integer> shape = new ArrayList<Integer>();
		try {
			
			Statement command = group.database.connection.createStatement();
			String commandText = "SELECT index_table FROM dbo.variables WHERE var_id="+this.id;
			ResultSet reader = command.executeQuery(commandText);
			reader.next();
			String indexTable = reader.getString(1);
			reader.close();
			
			commandText = "SELECT dim_index, MAX(key_max) FROM " + indexTable;
            if (position != null) {
                commandText += " WHERE";
                for (int i = 0; i < position.length; i++)
                {
                    if (i > 0)
                    {
                        commandText += " AND";
                    }
                    commandText += " var" + i + " = " + position[i];
                }
                commandText += " GROUP BY dim_index";
            }
            commandText += " ORDER BY dim_index";
				
			reader = command.executeQuery(commandText);
			while (reader.next()) {
				shape.add(reader.getInt(2)+1);
			}
			result = new int[this.listDimensions().size()- position.length];
			for (int i=0; i<shape.size(); i++) {
				result[i] = shape.get(i);
			}
			
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get variable shape",e);
		}
		return result;		
	}
	
	/**
	 * Returns the shape for this variable.
	 * @return variable shape
	 * @throws ActiveStorageException
	 */
	public int[] getShape() throws ActiveStorageException {
		int[] result = null;
		String commandText = 
			"SELECT dim_length FROM dbo.GetVarShape(?,?) ORDER BY dim_index";
		ArrayList<Integer> shape = new ArrayList<Integer>();
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, group.id);
			command.setString(2, name);
			ResultSet reader = command.executeQuery();
			while (reader.next()) {
				shape.add(reader.getInt(1));
			}
			result = new int[shape.size()];
			for (int i=0; i<shape.size(); i++) {
				result[i] = shape.get(i);
			}
			
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get variable shape",e);
		}
		return result;
	}
	
    private void gather(byte[] result, int[] origin, int[] shape) throws ActiveStorageException
    {   
    	List<Integer> chunks = new ArrayList<Integer>();
    	String dataTableName = null;
    	try {
    		String commandText = "SELECT index_table, data_table FROM variables WHERE var_id = ?";
    		PreparedStatement command = group.database.connection.prepareStatement(commandText);
    		command.setInt(1, this.id);
    		ResultSet rs = command.executeQuery();
    		rs.next();
    		String indexTableName = rs.getString(1);
    		dataTableName = rs.getString(2);
    		rs.close();
    		command.close();
    		
            String sql =
                    "SELECT ChunkKey" +
                    " FROM (SELECT Dimension0.chunk_key AS ChunkKey FROM ";
            
            int dimensionCount = origin.length;
            
            for (int i = 0; i < dimensionCount; i++)
            {
            	int min = origin[i];
            	int max = origin[i]+shape[i]-1;
                sql +=
                    "(SELECT chunk_key FROM " + indexTableName + " WHERE dim_index=" + i;

                sql +=
                    " AND (key_min BETWEEN " + min + " AND " + max +
                    " OR key_max BETWEEN " + min + " AND " + max +
                    " OR (key_min < " + min + " AND key_max > " + min + "))) Dimension" + i;
                if (i > 0)
                {
                    sql += " ON Dimension" + (i - 1) + ".chunk_key=Dimension" + i + ".chunk_key";
                }
                if (i < (dimensionCount - 1))
                {
                    sql += " JOIN ";
                }
                else
                {
                    sql += ") JoinedIndex";
                }
            }
            
            Statement st = group.database.connection.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
            	chunks.add(rs.getInt(1));
            }   
    	}
    	catch (Exception e) {
    		throw new ActiveStorageException("Could not retreive chunk list",e);
    	}
    	
    	processList(chunks,dataTableName,result,origin,shape);
    	
    	/*
    	List<String> servers = new ArrayList<String>();
        
        try {
	        String commandText =
	            "SELECT host,port,db_name,login,passwd FROM dbo.servers"+
	            " JOIN dbo.variables ON dbo.servers.var_id = dbo.variables.var_id "+
	            " WHERE var_name = ? AND var_group = ?";
	        PreparedStatement command = group.database.connection.prepareStatement(commandText);
	        command.setString(1, this.name);
	        command.setInt(2, group.id);
	        ResultSet reader = command.executeQuery();
	
	        while (reader.next())
	        {
	            String connectionString = 
	            "jdbc:sqlserver://"+reader.getString(1)+":"+reader.getString(2)+";"+
	            "databaseName="+reader.getString(3)+";"+
	            "user="+reader.getString(4)+";"+
	            "password="+reader.getString(5);
	            servers.add(connectionString);
	        }
	        
	        reader.close();
	        command.close();
	                
	        if (servers.size() > 0)
	        {
	
	            List<Thread> threads = new ArrayList<Thread>();
	            for(int i=0; i<servers.size(); i++)
	            {
	            	String connectionString = servers.get(i);
	                Thread thread = new DataWorker(result, group.getName(), this.name, origin, shape, connectionString);
	                threads.add(thread);
	                thread.start();
	            }
	            
	            for(int i=0; i<threads.size(); i++) 
	            {
	            	Thread thread = threads.get(i);
	                thread.join();
	            }
	        }
	        else
	            merge(result, origin, shape);
        }
        catch (Exception e) {
        	throw new ActiveStorageException("Could not gather data from servers",e);
        }
        */
    	
    	
        return;
    }
    
	private void merge(byte[] result, int[] origin, int[] shape) throws ActiveStorageException {
		final int MIN = 0;
		final int MAX = 1;

		String min = "";
		String max = "";
		String str = "";

		if (origin.length != shape.length) {
			throw new ActiveStorageException("Origin and size arrays are not the same length");
		}

		int dimensionCount = origin.length;
		int dataOffset = dimensionCount * 8;

		int[][] bounds = new int[dimensionCount][2];
		int[][] chunkBounds = new int[dimensionCount][2];
		int[] index = new int[dimensionCount];

		for (int i = 0; i < dimensionCount; i++) {
			min += origin[i];
			max += origin[i] + shape[i] - 1;
			str += "1";
			if (i != (dimensionCount-1)) {
				min += " ";
				max += " ";
				str += " ";
			}

			bounds[i][MIN] = origin[i];
			bounds[i][MAX] = origin[i] + shape[i] - 1;
		}

		try {
			String commandText = "EXEC dbo.GetData ?, ?, ?, ?, ?";
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, group.id);
			command.setString(2, this.name);
			command.setString(3, min);
			command.setString(4, max);
			command.setString(5, str);
			ResultSet reader = command.executeQuery();

			// walk through data chunks
			while (reader.next()) {
				byte[] chunk = reader.getBytes(2);
				if (chunk != null) {
					ByteArrayInputStream bytesInputStream = new ByteArrayInputStream(chunk);
					DataInputStream dataInputStream = new DataInputStream(bytesInputStream);

					for (int i = 0; i < dimensionCount; i++) {
						// read chunk header (bounds)
						chunkBounds[i][MIN] = dataInputStream.readInt();
						chunkBounds[i][MAX] = dataInputStream.readInt();

						// calculate relative chunk bounds
						chunkBounds[i][MIN] = (chunkBounds[i][MIN] - bounds[i][MIN]) /* /stride[i] */ ;	
						chunkBounds[i][MAX] = (chunkBounds[i][MAX] - bounds[i][MIN]) /* /stride[i] */ ;	

						index[i] = 0; // reset index
					}

					int dimension = dimensionCount - 1; // set current dimension to last
					int length = chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN] + 1; // contiguous byte block length

					// walk through contiguous byte blocks
					while (!(dimension < 0)) {
						int sourceOffset = dataOffset;
						int destinationOffset = 0;

						// calculate array offsets
						for (int i = 0; i < dimensionCount; i++) {
							int sourceIndex1D = index[i]; // source array 1D index
							int destinationIndex1D = sourceIndex1D + chunkBounds[i][MIN]; // destination array 1D index
							for (int j = i + 1; j < dimensionCount; j++) {
								sourceIndex1D *= chunkBounds[j][MAX] - chunkBounds[j][MIN] + 1;
								destinationIndex1D *= (bounds[j][MAX] - bounds[j][MIN]) /* /stride[j] */ +1;

							}
							sourceOffset += sourceIndex1D * type.getTotalSize(); // source array offset
							destinationOffset += destinationIndex1D * type.getTotalSize(); // destination array offset
						}

						System.arraycopy(chunk, sourceOffset, result, destinationOffset, length * type.getTotalSize());

						// move to next byte block
						if (dimension == (dimensionCount - 1))
							index[dimension] += length;
						else
							index[dimension]++;

						boolean reset = false;

						// reset index count if bounds reached
						while (index[dimension] > (chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN])) {
							reset = true;
							index[dimension] = 0;
							dimension--;
							if (dimension < 0) // last dimension processed, exit loop
							{
								reset = false;
								break;
							}
							index[dimension]++; // increase previous dimension index count
						}
						if (reset)
							dimension = dimensionCount - 1; // reset current dimension to last
					}
				}
			}
		} catch (Exception e) {
			throw new ActiveStorageException("Could not retrieve data",e);
		}
	}
	
	/**
	 * Returns a variable attribute with the given name.
	 * @param attributeName
	 * @return attribute
	 * @throws ActiveStorageException
	 */
	public Attribute getAttribute(String attributeName) throws ActiveStorageException {
		String commandText =
			"SELECT type_name, att_value FROM dbo.var_attributes JOIN dbo.types ON att_type = type_id WHERE var_id = ? AND att_name = ?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			command.setString(2, attributeName);
			ResultSet reader = command.executeQuery();
			if (reader.next()) {
				String typeName = reader.getString(1);
				String record = reader.getString(2);
				return Attribute.parse(attributeName, record, DataType.getCodeForName(typeName));
			}	
			else {
				return null;
				//throw new ActiveStorageException("Attribute does not exist");
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get attribute", e);
		}
	}
	
	/**
	 * Sets the given attribute.
	 * @param attribute
	 * @throws ActiveStorageException
	 */
	public void setAttribute(Attribute attribute) throws ActiveStorageException {
		String commandText =
			"EXEC dbo.SetVarAttribute ?,?,?,?,?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, group.id);
			command.setString(2, this.name);
			command.setString(3, attribute.getName());
			command.setString(4, attribute.getDataType().getName());
			command.setString(5, attribute.getString());
			command.execute();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not set attribute", e);
		}
	}
	
	/**
	 * Deletes an attribute with the given name.
	 * @param attributeName
	 * @throws ActiveStorageException
	 */
	public void deleteAttribute(String attributeName) throws ActiveStorageException {
		String commandText =
			"EXEC dbo.DeleteVarAttribute ?,?,?";
		try {
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, group.id);
			command.setString(2, this.name);
			command.setString(3, attributeName);
			command.execute();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not delete attribute", e);
		}
	}
	
	/**
	 * Automatically updates dimension length for this variable.
	 * @throws ActiveStorageException
	 */
	public void updateDimensions() throws ActiveStorageException {
		List<Dimension> dims = listDimensions();
		try {
			String commandText = 
				"SELECT index_table FROM dbo.variables WHERE var_id = ?";
			PreparedStatement command = group.database.connection.prepareStatement(commandText);
			command.setInt(1, this.id);
			ResultSet reader = command.executeQuery();
			reader.next();
			String table = reader.getString(1);
			
			reader.close(); command.close();
			commandText = 
				"SELECT MAX(key_max) FROM "+table+" WHERE dim_index = ?"; 
			command = group.database.connection.prepareStatement(commandText);
			for (int i=0; i<dims.size(); i++) {
				command.setInt(1, i);
				reader = command.executeQuery();
				int length = 0;
				if (reader.next()) {
					length = reader.getInt(1) + 1;
				}
				reader.close();
				Dimension dim =  dims.get(i);
				dim.setLength(length);
			}
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not update dimension lengths",e);
		}
	} 

	private class DataWorker extends Thread {
	    private byte[] result;
	    private int[] origin;
	    private int[] shape;
	    private String groupName;
	    private String variableName;
	    private String connectionString;

	    public DataWorker(byte[] result, String groupName, String variableName, int[] origin, int[] shape, String connectionString)
	    {
	    	super();
	    	
	        this.result = result;
	        this.origin = origin;
	        this.shape = shape;

	        this.groupName = groupName;
	        this.variableName = variableName;
	        this.connectionString = connectionString;
	    }

	    public void run() 
	    {
		    Database dataset = null;
		    Group group = null;
		    Variable variable = null;
		    
	    	try 
	    	{
		        dataset = new Database(connectionString); //SWIFT
		    	group = dataset.getGroup(groupName);
		    	variable = group.getVariable(variableName);
	    		variable.merge(result, origin, shape);
	        }
	    	catch (Exception e) 
	    	{
	    		throw new RuntimeException("Job failed", e);
	    	}
	    	finally 
	    	{
	    		try 
	    		{
	    			dataset.close();
	    		}
	    		catch (Exception e) {}
	    	}
	    }  	
	}

	// -------------- //
	// --- MATLAB --- //
	// -------------- //
	
    public byte[][][][][] getDataByteM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];

    	byte[] buf = getDataByte(start,count);
    	byte[][][][][] result = new byte[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }
    
    public short[][][][][] getDataShortM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];

    	short[] buf = getDataShort(start,count);
    	short[][][][][] result = new short[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }
    
    public int[][][][][] getDataIntM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];
    	
    	int[] buf = getDataInt(start,count);
    	int[][][][][] result = new int[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }
    
    public long[][][][][] getDataLongM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];

    	long[] buf = getDataLong(start,count);
    	long[][][][][] result = new long[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }
    
    public float[][][][][] getDataFloatM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];

    	float[] buf = getDataFloat(start,count);
    	float[][][][][] result = new float[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }

    public double[][][][][] getDataDoubleM(int[] start, int[] count) throws Exception 
    {
    	int[] size = new int[] {1,1,1,1,1};
    	for (int i=0; i<count.length; i++) size[i]=count[i];
    	
    	double[] buf = getDataDouble(start,count);
    	double[][][][][] result = new double[size[0]][size[1]][size[2]][size[3]][size[4]];
    	int i = 0;
        for(int d0 = 0; d0<size[0]; d0++)
        	for(int d1 = 0; d1<size[1]; d1++)
        		for(int d2=0; d2<size[2]; d2++)
        			for(int d3=0; d3<size[3]; d3++)
        				for(int d4=0;d4<size[4]; d4++) 
        				{
        					result[d0][d1][d2][d3][d4] = buf[i];
        					i++;
        				}
        return result;
    }
    
    public int putDataByteM(int[] start, int[] count, byte[][][][][] b) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	byte[] buf = new byte[size];
    	int i = 0;
    	for (int d0=0; d0<b.length; d0++)
    		for (int d1=0; d1<b[0].length; d1++)
    			for (int d2=0; d2<b[0][0].length; d2++)
    				for (int d3=0; d3<b[0][0][0].length; d3++)
    					for (int d4=0; d4<b[0][0][0][0].length; d4++)
    					{
    						buf[i] = b[d0][d1][d2][d3][d4];
    						i++;
    					}
    	
    	putDataByte(start,count,buf);
    	return 0;
    }
    
    public int putDataShortM(int[] start, int[] count, short[][][][][] s) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	short[] buf = new short[size];
    	int i = 0;
    	for (int d0=0; d0<s.length; d0++)
    		for (int d1=0; d1<s[0].length; d1++)
    			for (int d2=0; d2<s[0][0].length; d2++)
    				for (int d3=0; d3<s[0][0][0].length; d3++)
    					for (int d4=0; d4<s[0][0][0][0].length; d4++)
    					{
    						buf[i] = s[d0][d1][d2][d3][d4];
    						i++;
    					}
    	
    	putDataShort(start,count,buf);
    	return 0;
    }

    public int putDataLongM(int[] start, int[] count, long[][][][][] l) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	long[] buf = new long[size];
    	int i = 0;
    	for (int d0=0; d0<l.length; d0++)
    		for (int d1=0; d1<l[0].length; d1++)
    			for (int d2=0; d2<l[0][0].length; d2++)
    				for (int d3=0; d3<l[0][0][0].length; d3++)
    					for (int d4=0; d4<l[0][0][0][0].length; d4++)
    					{
    						buf[i] = l[d0][d1][d2][d3][d4];
    						i++;
    					}
    	
    	putDataLong(start,count,buf);
    	return 0;
    }

    public int putDataIntM(int[] start, int[] count, int[][][][][] i) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	int[] buf = new int[size];
    	int j = 0;
    	for (int d0=0; d0<i.length; d0++)
    		for (int d1=0; d1<i[0].length; d1++)
    			for (int d2=0; d2<i[0][0].length; d2++)
    				for (int d3=0; d3<i[0][0][0].length; d3++)
    					for (int d4=0; d4<i[0][0][0][0].length; d4++)
    					{
    						buf[j] = i[d0][d1][d2][d3][d4];
    						j++;
    					}
    	
    	putDataInt(start,count,buf);
    	return 0;
    }

    public int putDataFloatM(int[] start, int[] count, float[][][][][] f) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	float[] buf = new float[size];
    	int i = 0;
    	for (int d0=0; d0<f.length; d0++)
    		for (int d1=0; d1<f[0].length; d1++)
    			for (int d2=0; d2<f[0][0].length; d2++)
    				for (int d3=0; d3<f[0][0][0].length; d3++)
    					for (int d4=0; d4<f[0][0][0][0].length; d4++)
    					{
    						buf[i] = f[d0][d1][d2][d3][d4];
    						i++;
    					}
    	
    	putDataFloat(start,count,buf);
    	return 0;
    }

    public int putDataDoubleM(int[] start, int[] count, double[][][][][] d) throws Exception 
    {
    	int size = 1;
    	for (int n=0; n<count.length; n++) {
    		size *= count[n];
    	}
    	
    	double[] buf = new double[size];
    	int i = 0;
    	for (int d0=0; d0<d.length; d0++)
    		for (int d1=0; d1<d[0].length; d1++)
    			for (int d2=0; d2<d[0][0].length; d2++)
    				for (int d3=0; d3<d[0][0][0].length; d3++)
    					for (int d4=0; d4<d[0][0][0][0].length; d4++)
    					{
    						buf[i] = d[d0][d1][d2][d3][d4];
    						i++;
    					}
    	
    	putDataDouble(start,count,buf);
    	return 0;
    }
    
    public static final byte[] intToByteArray(int value) {
        /*
    	return new byte[] {
                (byte)(value),
                (byte)(value >>> 8),
                (byte)(value >>> 16),
                (byte)(value >>> 24)};
        */
    	return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)(value)};
    }
    
    private void process(byte[] chunk, int[] origin, int[] shape, byte[] result) throws ActiveStorageException {
    	if (chunk == null) throw new ActiveStorageException("Data chunk cannot be null");
    	if (origin == null) throw new ActiveStorageException("Origin cannot be null");
    	if (shape == null) throw new ActiveStorageException("Shape cannot be null");
    	if (origin.length != shape.length) throw new ActiveStorageException("Origin and shape dimensons must be equal");
    	
    	int dimensionCount = origin.length;
    	int MIN = 0, MAX = 1;
    	int[][] bounds = new int[dimensionCount][2];
    	int[][] chunkBounds = new int[dimensionCount][2];
    	int[] index = new int[dimensionCount];
    	int dataOffset = dimensionCount * 8;
    	
    	try {
			ByteArrayInputStream bytesInputStream = new ByteArrayInputStream(chunk);
			DataInputStream dataInputStream = new DataInputStream(bytesInputStream);
	
			for (int i = 0; i < dimensionCount; i++) {
				// read chunk header (bounds)
				chunkBounds[i][MIN] = dataInputStream.readInt();
				chunkBounds[i][MAX] = dataInputStream.readInt();
				bounds[i][MIN] = origin[i];
				bounds[i][MAX] = origin[i] + shape[i] - 1;

				// calculate relative chunk bounds
				chunkBounds[i][MIN] = (chunkBounds[i][MIN] - bounds[i][MIN]) /* /stride[i] */ ;	
				chunkBounds[i][MAX] = (chunkBounds[i][MAX] - bounds[i][MIN]) /* /stride[i] */ ;	

				index[i] = 0; // reset index
			}
			
			int dimension = dimensionCount - 1; // set current dimension to last
			int length = chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN] + 1; // contiguous byte block length

			// walk through contiguous byte blocks
			while (!(dimension < 0)) {
				int sourceOffset = dataOffset;
				int destinationOffset = 0;

				// calculate array offsets
				for (int i = 0; i < dimensionCount; i++) {
					int sourceIndex1D = index[i]; // source array 1D index
					int destinationIndex1D = sourceIndex1D + chunkBounds[i][MIN]; // destination array 1D index
					for (int j = i + 1; j < dimensionCount; j++) {
						sourceIndex1D *= chunkBounds[j][MAX] - chunkBounds[j][MIN] + 1;
						destinationIndex1D *= (bounds[j][MAX] - bounds[j][MIN]) /* /stride[j] */ +1;

					}
					sourceOffset += sourceIndex1D * type.getTotalSize(); // source array offset
					destinationOffset += destinationIndex1D * type.getTotalSize(); // destination array offset
				}

				if (destinationOffset>=0 && (destinationOffset+type.getTotalSize() <= result.length))
					System.arraycopy(chunk, sourceOffset, result, destinationOffset, 1*type.getTotalSize());

				// move to next byte block
				/*if (dimension == (dimensionCount - 1))
					index[dimension] += length;
				else */
					index[dimension]++;

				boolean reset = false;

				// reset index count if bounds reached
				while (index[dimension] > (chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN])) {
					reset = true;
					index[dimension] = 0;
					dimension--;
					if (dimension < 0) // last dimension processed, exit loop
					{
						reset = false;
						break;
					}
					index[dimension]++; // increase previous dimension index count
				}
				if (reset)
					dimension = dimensionCount - 1; // reset current dimension to last
			}
    	}
    	catch (Exception e) {
    		throw new ActiveStorageException("Could not process data chunk",e);
    	}
    }

    private void processList(List<Integer> chunks, String dataTableName, byte[] result, int[] origin, int[] shape) throws ActiveStorageException {
    	try {
	    	Iterator<Integer> it = chunks.iterator();
	    	ExecutorService executor = Executors.newFixedThreadPool(5);
	    	List<Callable<Object>> tasks = new ArrayList<Callable<Object>>();

	    	while (it.hasNext()) {
	    		int key = it.next();
	    		
	    		/*
	    		byte[] chunk = group.database.swiftClient.getObject(dataTableName, "data_"+key);
	    		process(chunk,origin,shape,result);
	    		*/
	    		tasks.add(Executors.callable(new MyThread(dataTableName,type,key,group,origin,shape,result)));
	    	}
	    	executor.invokeAll(tasks);
	    	executor.shutdown();
    	}
    	catch (Exception e) {
    		throw new ActiveStorageException("Could not retrieve chunk data",e);
    	}
    }
    
    
 }

	class MyThread implements Runnable {
		private Group group;
		private String dataTableName;
		private int[] origin;
		private int[] shape;
		private byte[] result;
		private int key;
		private DataType type;
		
	  MyThread(String dataTableName, DataType type, int key, Group group, int[] origin, int[] shape, byte[] result) {
		  this.dataTableName = dataTableName;
		  this.group = group;
		  this.origin = origin;
		  this.shape = shape;
		  this.result = result;
		  this.key = key;
		  this.type = type;
	  }

	  public void run() throws RuntimeException {
  		try {
  		  ObjectResource object = group.database.swiftClient.getObject(dataTableName, "data_"+key);
		  // byte[] chunk = object.head();
  		  // process(chunk,origin,shape,result);
  		}
  		catch (Exception e) {
  			throw new RuntimeException(e);
  		}
	  }
	  
	    private void process(byte[] chunk, int[] origin, int[] shape, byte[] result) throws ActiveStorageException {
	    	if (chunk == null) throw new ActiveStorageException("Data chunk cannot be null");
	    	if (origin == null) throw new ActiveStorageException("Origin cannot be null");
	    	if (shape == null) throw new ActiveStorageException("Shape cannot be null");
	    	if (origin.length != shape.length) throw new ActiveStorageException("Origin and shape dimensons must be equal");
	    	
	    	int dimensionCount = origin.length;
	    	int MIN = 0, MAX = 1;
	    	int[][] bounds = new int[dimensionCount][2];
	    	int[][] chunkBounds = new int[dimensionCount][2];
	    	int[] index = new int[dimensionCount];
	    	int dataOffset = dimensionCount * 8;
	    	
	    	try {
				ByteArrayInputStream bytesInputStream = new ByteArrayInputStream(chunk);
				DataInputStream dataInputStream = new DataInputStream(bytesInputStream);
		
				for (int i = 0; i < dimensionCount; i++) {
					// read chunk header (bounds)
					chunkBounds[i][MIN] = dataInputStream.readInt();
					chunkBounds[i][MAX] = dataInputStream.readInt();
					bounds[i][MIN] = origin[i];
					bounds[i][MAX] = origin[i] + shape[i] - 1;

					// calculate relative chunk bounds
					chunkBounds[i][MIN] = (chunkBounds[i][MIN] - bounds[i][MIN]) /* /stride[i] */ ;	
					chunkBounds[i][MAX] = (chunkBounds[i][MAX] - bounds[i][MIN]) /* /stride[i] */ ;	

					index[i] = 0; // reset index
				}
				
				int dimension = dimensionCount - 1; // set current dimension to last
				int length = chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN] + 1; // contiguous byte block length

				// walk through contiguous byte blocks
				while (!(dimension < 0)) {
					int sourceOffset = dataOffset;
					int destinationOffset = 0;

					// calculate array offsets
					for (int i = 0; i < dimensionCount; i++) {
						int sourceIndex1D = index[i]; // source array 1D index
						int destinationIndex1D = sourceIndex1D + chunkBounds[i][MIN]; // destination array 1D index
						for (int j = i + 1; j < dimensionCount; j++) {
							sourceIndex1D *= chunkBounds[j][MAX] - chunkBounds[j][MIN] + 1;
							destinationIndex1D *= (bounds[j][MAX] - bounds[j][MIN]) /* /stride[j] */ +1;

						}
						sourceOffset += sourceIndex1D * type.getTotalSize(); // source array offset
						destinationOffset += destinationIndex1D * type.getTotalSize(); // destination array offset
					}

					if (destinationOffset>=0 && (destinationOffset+type.getTotalSize() <= result.length))
						System.arraycopy(chunk, sourceOffset, result, destinationOffset, 1*type.getTotalSize());

					// move to next byte block
					/*if (dimension == (dimensionCount - 1))
						index[dimension] += length;
					else */
						index[dimension]++;

					boolean reset = false;

					// reset index count if bounds reached
					while (index[dimension] > (chunkBounds[dimension][MAX] - chunkBounds[dimension][MIN])) {
						reset = true;
						index[dimension] = 0;
						dimension--;
						if (dimension < 0) // last dimension processed, exit loop
						{
							reset = false;
							break;
						}
						index[dimension]++; // increase previous dimension index count
					}
					if (reset)
						dimension = dimensionCount - 1; // reset current dimension to last
				}
	    	}
	    	catch (Exception e) {
	    		throw new ActiveStorageException("Could not process data chunk",e);
	    	}
	    }

}

