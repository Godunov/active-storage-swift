package ru.wdcb.activestorage;

import java.sql.*;
import java.util.Properties;

import org.openstack.api.storage.AccountResource;
import org.openstack.client.OpenStackClient;

import ru.wdcb.swift.client.SwiftClient;

/**
 * An ActiveStorage database.
 * @author  dmedv
 */
public class Database {

	protected Connection connection;
	/**
	 * @uml.property  name="swiftClient"
	 * @uml.associationEnd  
	 */
	public SwiftClient swiftClient;
	
	/**
	 * Opens a database for the given connection string.
	 * @param connectionString JDBC connection string. For example: 
	 * <code>"jdbc:mysql://localhost/ActiveStorage?user=guest&password=xxx"</code>.
	 * @throws ActiveStorageException
	 */
	/*public Database(String connectionString, String authUrl, String authUser, String authKey) throws ActiveStorageException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(connectionString);
			swiftClient = new SwiftClient(authUrl,authUser,authKey);
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not initialize dataset",e);
		}
	}*/
	
	public Database(String connectionString) throws ActiveStorageException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(connectionString);
			swiftClient = new SwiftClient();
			//System.out.println(swiftClient.storage.get());
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not initialize dataset",e);
		}
	}

	/**
	 * Returns the root group for the database.
	 * @return <code>Group</code> object
	 * @throws ActiveStorageException
	 */
	public Group getRootGroup() throws ActiveStorageException {
		return getGroup("");
	}
	
	/**
	 * Returns a group with the given name.
	 * @param groupName hierarchical group name. Hierarchy levels are delimited by <code>"/"</code>.
	 * @return <code>Group</code> object
	 * @throws ActiveStorageException
	 */
	public Group getGroup(String groupName) throws ActiveStorageException {
		int groupId = -1;
		String[] groupNames = groupName.split("//");
		try {
			String commandText = 
					"SELECT group_id FROM groups WHERE parent_id IS NULL";
			Statement command = connection.prepareStatement(commandText);
	        ResultSet reader = command.executeQuery(commandText);
	        reader.next();
	        groupId = reader.getInt(1);
	        reader.close();
	        command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not root group id",e);
		}
		
		if (!(groupName.equals("")))
		try {
			String commandText = 
					"SELECT group_id FROM groups WHERE group_name = ? AND parent_id = ?";
			PreparedStatement command = connection.prepareStatement(commandText);
			for (int i = 0; i < groupNames.length; i++) {
				command.setString(1,groupNames[i]);
				command.setInt(2,groupId);
		        ResultSet reader = command.executeQuery();
		        reader.next();
		        groupId = reader.getInt(1);
		        reader.close();

			}
	        command.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not get group id",e);
		}
		
		if (groupId < 0) {
			return null;
		}
		else {
			return new Group(this,groupId);
		}
	}
	
	/**
	 * Closes the database.
	 * @throws ActiveStorageException
	 */
	public void close() throws ActiveStorageException {
		try {
			connection.close();
		}
		catch (Exception e) {
			throw new ActiveStorageException("Could not close dataset",e);
		}
	}
}
