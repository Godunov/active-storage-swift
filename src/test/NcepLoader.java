package test;

import java.io.FileInputStream;
import java.util.Properties;
import ru.wdcb.activestorage.DataType;
import ru.wdcb.activestorage.Database;
import ru.wdcb.activestorage.Dimension;
import ru.wdcb.activestorage.Group;
import ru.wdcb.activestorage.Variable;
import ucar.nc2.NetcdfFile;

public class NcepLoader {

	int year1 = 2010;
	int year2 = 2012;
	int timeStep = 1500;
	int latStep = 10;
	int lonStep = 10;
	String prefix = "D:\\NetCDF\\";
	
	public void run() throws Exception {
		Properties p = new Properties();
		p.load(new FileInputStream("config.properties"));
		String connectionString = p.getProperty("database.url");
		Database db = new Database(connectionString);
		
		Group group = db.getRootGroup().createSubgroup("surface");
		
		Dimension time = group.createDimension("time");
		Dimension lat = group.createDimension("lat");
		Dimension lon = group.createDimension("lon");
		
		Variable dataVar = group.createVariable("air_sig995", new DataType(DataType.SHORT), new Dimension[] {time,lat,lon});
		Variable timeVar = group.createVariable("time", new DataType(DataType.DOUBLE), new Dimension[] {time});
		Variable latVar = group.createVariable("lat", new DataType(DataType.FLOAT), new Dimension[] {lat});
		Variable lonVar = group.createVariable("lon", new DataType(DataType.FLOAT), new Dimension[] {lon});
		
		//Group group = db.getRootGroup().getSubgroup("surface");
		//Variable timeVar = group.getVariable("time");
		
		int globalTime = 0;
		
		for (int year=year1; year<=year2; year++) {
			System.out.println("Year: "+year);
			NetcdfFile nc;
			try {
				nc = NetcdfFile.open(prefix+"air.sig995."+year+".nc");
			} catch (Exception e) {
				continue;
			}

			int latLen = nc.findDimension("lat").getLength();
			int lonLen = nc.findDimension("lon").getLength();
			int timeLen = nc.findDimension("time").getLength();
			ucar.nc2.Variable ncData = nc.findVariable("air");
			ucar.nc2.Variable ncTime = nc.findVariable("time");

			
			if (year == year1) {
				ucar.nc2.Variable ncLat = nc.findVariable("lat");
				ucar.nc2.Variable ncLon = nc.findVariable("lon");
				float[] latData = (float[])ncLat.read().copyTo1DJavaArray();
				float[] lonData = (float[])ncLon.read().copyTo1DJavaArray();
				latVar.putData(new int[]{0}, new int[]{latData.length}, latData);
				lonVar.putData(new int[]{0}, new int[]{lonData.length}, lonData);
			}
			
			
			int currentTime = 0;
			while (currentTime < timeLen) {
				int currentTimeStep = (currentTime + timeStep > timeLen)?(timeLen-currentTime):timeStep;				
				int currentLat = 0;
				
				double[] timeData = (double[])ncTime.read(new int[]{currentTime},new int[]{currentTimeStep}).copyTo1DJavaArray();
				timeVar.putData(new int[] {globalTime}, new int[] {currentTimeStep}, timeData);

				
				while (currentLat < latLen) {
					int currentLatStep = (currentLat + latStep > latLen)?(latLen-currentLat):latStep; 
					int currentLon = 0;
					
					while (currentLon < lonLen) {
						int currentLonStep = (currentLon + lonStep > lonLen)?(lonLen-currentLon):lonStep;
						
						int[] origin = new int[] {currentTime, currentLat, currentLon};
						int[] shape = new int[] {currentTimeStep, currentLatStep, currentLonStep};
						short[] data = (short[])ncData.read(origin, shape).copyTo1DJavaArray();
						
						dataVar.putData(new int[] {globalTime, currentLat, currentLon}, shape, data);
						currentLon += currentLonStep;
					}
					
					currentLat += currentLatStep;
				}
				
				
				currentTime += currentTimeStep;
				globalTime += currentTimeStep;
			}
			
			nc.close();
			
		}
	
		db.close();
	}
	
	public static void main(String[] args) throws Exception {

		NcepLoader instance = new NcepLoader();
		instance.run();
	}

}

