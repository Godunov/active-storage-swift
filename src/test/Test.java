package test;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.wdcb.activestorage.DataType;
import ru.wdcb.activestorage.Database;
import ru.wdcb.activestorage.Dimension;
import ru.wdcb.activestorage.Group;
import ru.wdcb.activestorage.Variable;
import ru.wdcb.swift.client.SwiftClient;
import ucar.nc2.NetcdfFile;

public class Test {

	String databaseUrl;
	
	Database db;
	final Properties p = new Properties();
	
	public void configure() throws Exception {
		p.load(new FileInputStream("config.properties"));
		databaseUrl = p.getProperty("database.url");
	}
	
	public void connect() throws Exception {
		db = new Database(databaseUrl);		
	}
	
	public void done() throws Exception {
		db.close();
	}
	
	public void createTestGroup() throws Exception {
		db.getRootGroup().createSubgroup("test");
	}
	
	public void deleteTestGroup() throws Exception {
		Group group = db.getRootGroup().getSubgroup("test");
		
		//Delete all variables
		List<Variable> variables = group.listVariables();
		System.out.println(variables);
		Iterator<Variable> vi = variables.iterator();
		while(vi.hasNext()) {
			Variable v = vi.next();
			v.delete();
		}
		
		//Delete all dimensions
		List<Dimension> dimensions = group.listDimensions();
		Iterator<Dimension> di = dimensions.iterator();
		while(di.hasNext()) {
			Dimension d = di.next();
			d.delete();
		}
		
		group.delete();
	}

	public void createNcepMetadata3D() throws Exception {
		Group group = db.getRootGroup().getSubgroup("test");
		Dimension time = group.createDimension("time");
		Dimension lat = group.createDimension("lat");
		Dimension lon = group.createDimension("lon");
		group.createVariable("air_sig995", new DataType(DataType.SHORT), new Dimension[] {time,lat,lon});
		group.createVariable("time", new DataType(DataType.DOUBLE), new Dimension[] {time});
		group.createVariable("lat", new DataType(DataType.FLOAT), new Dimension[] {lat});
		group.createVariable("lon", new DataType(DataType.FLOAT), new Dimension[] {lon});
	}
	
	public void loadNcepData3D() throws Exception {
		String prefix = "D:\\NetCDF\\";
		int year1 = 1948;
		int year2 = 2012;
		int timeStep = 1000;
		int latStep = 10;
		int lonStep = 10;
		
		List<Double> time = new ArrayList<Double>();
		
		int globalTime = 0;
		
		for (int year=year1; year<=year2; year++) {
			System.out.print("Year: "+year);
			
			NetcdfFile nc = NetcdfFile.open(prefix+"air.sig995."+year+".nc");
			int latLen = nc.findDimension("lat").getLength();
			int lonLen = nc.findDimension("lon").getLength();
			int timeLen = nc.findDimension("time").getLength();
			ucar.nc2.Variable ncData = nc.findVariable("air");
			ucar.nc2.Variable ncTime = nc.findVariable("time");

			Group group = db.getRootGroup().getSubgroup("test");
			Variable dataVar = group.getVariable("air_sig995");
			Variable timeVar = group.getVariable("time");
			Variable latVar = group.getVariable("lat");
			Variable lonVar = group.getVariable("lon");
			
			if (year == year1) {
				ucar.nc2.Variable ncLat = nc.findVariable("lat");
				ucar.nc2.Variable ncLon = nc.findVariable("lon");
				float[] latData = (float[])ncLat.read().copyTo1DJavaArray();
				float[] lonData = (float[])ncLon.read().copyTo1DJavaArray();
				latVar.putData(new int[]{0}, new int[]{latData.length}, latData);
				lonVar.putData(new int[]{0}, new int[]{lonData.length}, lonData);
			}
			
			long dt = 0;
			
			int currentTime = 0;
			while (currentTime < timeLen) {
				int currentTimeStep = (currentTime + timeStep > timeLen)?(timeLen-currentTime):timeStep;				
				int currentLat = 0;
				
				double[] timeData = (double[])ncTime.read(new int[]{currentTime},new int[]{currentTimeStep}).copyTo1DJavaArray();
				timeVar.putData(new int[] {globalTime}, new int[] {currentTimeStep}, timeData);

				
				while (currentLat < latLen) {
					int currentLatStep = (currentLat + latStep > latLen)?(latLen-currentLat):latStep; 
					int currentLon = 0;
					
					while (currentLon < lonLen) {
						int currentLonStep = (currentLon + lonStep > lonLen)?(lonLen-currentLon):lonStep;
						
						int[] origin = new int[] {currentTime, currentLat, currentLon};
						int[] shape = new int[] {currentTimeStep, currentLatStep, currentLonStep};
						short[] data = (short[])ncData.read(origin, shape).copyTo1DJavaArray();
						
						long t1 = Calendar.getInstance().getTimeInMillis();
						dataVar.putData(new int[] {globalTime, currentLat, currentLon}, shape, data);
						long t2 = Calendar.getInstance().getTimeInMillis();
						dt += (t2-t1);
						currentLon += currentLonStep;
					}
					
					currentLat += currentLatStep;
				}
				
				currentTime += currentTimeStep;
				globalTime += currentTimeStep;
			}			
			nc.close();
			
			double t = dt/1000.0;
			System.out.println(", time = "+t);
			time.add(t);
		}

		double total = 0;
		for (int i=0; i<time.size(); i++) total+=time.get(i);
		System.out.println("Total time: "+total);
		System.out.println("Average time: "+total/time.size());
	}
	
	public void loadNcepData3DParallel() throws Exception {
    	
    	List<Callable<Object>> tasks = new ArrayList<Callable<Object>>();
		
		String prefix = "D:\\NetCDF\\";
		int year1 = 1948;
		int year2 = 1968;
		//int year2 = 2012;
		int timeStep = 1000;
		int latStep = 10;
		int lonStep = 10;
		
		List<Double> time = new ArrayList<Double>();
		
		int globalTime = 0;
		
		for (int year=year1; year<=year2; year++) {
			ExecutorService executor = Executors.newFixedThreadPool(8);
			System.out.print("Year: "+year);
			
			NetcdfFile nc = NetcdfFile.open(prefix+"air.sig995."+year+".nc");
			int latLen = nc.findDimension("lat").getLength();
			int lonLen = nc.findDimension("lon").getLength();
			int timeLen = nc.findDimension("time").getLength();
			ucar.nc2.Variable ncData = nc.findVariable("air");
			ucar.nc2.Variable ncTime = nc.findVariable("time");

			Group group = db.getRootGroup().getSubgroup("test");
			Variable dataVar = group.getVariable("air_sig995");
			Variable timeVar = group.getVariable("time");
			Variable latVar = group.getVariable("lat");
			Variable lonVar = group.getVariable("lon");
			
			if (year == year1) {
				ucar.nc2.Variable ncLat = nc.findVariable("lat");
				ucar.nc2.Variable ncLon = nc.findVariable("lon");
				float[] latData = (float[])ncLat.read().copyTo1DJavaArray();
				float[] lonData = (float[])ncLon.read().copyTo1DJavaArray();
				latVar.putData(new int[]{0}, new int[]{latData.length}, latData);
				lonVar.putData(new int[]{0}, new int[]{lonData.length}, lonData);
			}
			
			
			
			int currentTime = 0;
			while (currentTime < timeLen) {
				int currentTimeStep = (currentTime + timeStep > timeLen)?(timeLen-currentTime):timeStep;				
				int currentLat = 0;
				
				double[] timeData = (double[])ncTime.read(new int[]{currentTime},new int[]{currentTimeStep}).copyTo1DJavaArray();
				timeVar.putData(new int[] {globalTime}, new int[] {currentTimeStep}, timeData);

				
				while (currentLat < latLen) {
					int currentLatStep = (currentLat + latStep > latLen)?(latLen-currentLat):latStep; 
					int currentLon = 0;
					
					while (currentLon < lonLen) {
						int currentLonStep = (currentLon + lonStep > lonLen)?(lonLen-currentLon):lonStep;
						
						int[] origin = new int[] {currentTime, currentLat, currentLon};
						int[] shape = new int[] {currentTimeStep, currentLatStep, currentLonStep};
						short[] data = (short[])ncData.read(origin, shape).copyTo1DJavaArray();
						
						tasks.add(Executors.callable(new MyThread(dataVar,new int[] {globalTime, currentLat, currentLon},shape,data)));
						//dataVar.putData(new int[] {globalTime, currentLat, currentLon}, shape, data);

						
						currentLon += currentLonStep;
					}
					
					currentLat += currentLatStep;
				}
				
				currentTime += currentTimeStep;
				globalTime += currentTimeStep;
			}			
			nc.close();
			
			long t1 = Calendar.getInstance().getTimeInMillis();
			executor.invokeAll(tasks);
			long t2 = Calendar.getInstance().getTimeInMillis();
			double t = (t2-t1)/1000.0;
			System.out.println(", time = "+t);
			time.add(t);
			
			executor.shutdown();
			tasks.clear();
			

		}

		double total = 0;
		for (int i=0; i<time.size(); i++) total+=time.get(i);
		System.out.println("Total time: "+total);
		System.out.println("Average time: "+total/time.size());
	}
	
	public void readNcepData3D() throws Exception {
		List<Double> time = new ArrayList<Double>();
		Group group = db.getRootGroup().getSubgroup("test");
		Variable dataVar = group.getVariable("air_sig995");
		
		
		int n = 10;
		short[] data = null;
		for (int i=0;i<n;i++) {
			long t1 = Calendar.getInstance().getTimeInMillis();
			data = (short[])dataVar.getData(new int[] {0,0,0}, new int[]{10000,25,100});
			long t2 = Calendar.getInstance().getTimeInMillis();
			time.add((t2-t1)/1000.0);
			System.out.println("Time #"+i+": "+(t2-t1)/1000.0);
		}
		
		double total = 0;
		for (int i=0; i<time.size(); i++) total+=time.get(i);
		
		System.out.println("Average time: "+total/time.size());
		System.out.println("Size: "+data.length*2);
		System.out.println("Average speed: "+data.length*2/(total/time.size()));
	}
	
	public static void main(String[] args) throws Exception {
		Test instance = new Test();
		instance.configure();
		instance.connect();
		instance.createTestGroup();
		instance.loadNcepData3D();
		instance.createNcepMetadata3D();
		instance.loadNcepData3DParallel();
		instance.deleteTestGroup();
		/*instance.readNcepData3D();
		instance.db.swiftClient.createContainer("test");*/
		
		instance.done();
	}
}


class MyThread implements Runnable {
	private Variable var;
	private int[] origin;
	private int[] shape;
	private short[] data;
	
  MyThread(Variable var, int[] origin, int[] shape, short[] data) {
	  this.var = var;
	  this.origin = origin;
	  this.shape = shape;
	  this.data = data;
  }

  public void run() throws RuntimeException {
		try {
			var.putData(origin, shape, data);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
  }
}