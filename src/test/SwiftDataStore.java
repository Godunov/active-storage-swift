package test;

import java.io.BufferedInputStream;
import java.util.Properties;
import org.jdom.*;
import org.openstack.api.storage.AccountResource;
import org.openstack.api.storage.ContainerResource;
import org.openstack.client.OpenStackClient;

public class SwiftDataStore {
  private String publicURL = null;
  private String container = null;
  private String objectID = null;
  private String tenantName = null;
  private String userName = null;
  private String password = null;
  public OpenStackClient openstack;
  public AccountResource storage; 

  public SwiftDataStore(String publicURL, String container, String objectID,
                        String tenantName, String userName, String password) {
    this.publicURL = publicURL;
    this.container = container;
    this.tenantName = tenantName;
    this.userName = userName;
    this.password = password;
    this.objectID = objectID;
	Properties p = new Properties();
	p.put("identity.endpoint.publicURL", this.publicURL);
	p.put("auth.credentials", "passwordCredentials");
	p.put("auth.username", this.userName);
	p.put("auth.password", this.password);
	p.put("auth.tenantName", this.tenantName);
	
	openstack = OpenStackClient.authenticate(p); // Keystone authentication
	storage = openstack.getStorageEndpoint();    // Access to Swift Storage - endpoint
  }

  public java.io.InputStream getData() {
	ContainerResource resource = storage.container(this.container);
	return resource.object(objectID).openStream();
  }

  public Element getMetadata() {
    return null;
  }
  
  /**
   * ��������� main() ������ ��� �������� ����������������� ����!!!
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
		SwiftDataStore sds = new SwiftDataStore("http://144.206.106.188:5000/v2.0", 
				"test", "test_object", "cloud", "maria", "333r-");
		BufferedInputStream bin = new BufferedInputStream(sds.getData());
		 int b;
		 while ( ( b = bin.read() ) != -1 )
		 {
      		     System.out.print(""+(char)b); //This prints out content that is unreadable.
		                                   //Isn't it supposed to print out html tag?
		 }
  }

}
