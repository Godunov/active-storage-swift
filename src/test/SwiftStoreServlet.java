package vobs.webapp;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;

import org.apache.commons.logging.*;
import wdc.settings.Settings;
import vobs.dbaccess.*;
import vobs.datamodel.*;
import java.util.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.*;
import vobs.store.*;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.modules.XMLResource;
import org.jdom.input.DOMBuilder;
import org.exist.xmldb.XQueryService;
import wdc.dbaccess.ConnectionPool;
import java.sql.Statement;
import wdc.utils.Basket;
import java.sql.Connection;
import org.xmldb.api.base.*;

public class SwiftStoreServlet
    extends HttpServlet {
  private static Log log = LogFactory.getLog(SwiftStoreServlet.class);
  private static String forumDB = Settings.get("vo_meta.forumResource");

  public static boolean objectExists(String ID) {
    try {
      DataStore ds = new DataStore(Settings.get("vo_store.fileStoreDir"));
      if (null == ID || ID.length() == 0)
        return false;

      return ds.dataExists(ID);
    }
    catch (DataStoreException ex) {
      log.error("Error creating new DataStore: " + ex.getMessage());
      ex.printStackTrace();
      return false;
    }
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {
    response.setHeader("Accept-Ranges", "bytes");
    String ID = request.getParameter("objId");
    if (null == ID) {
      ID = (String) request.getAttribute("objId");
    }

    if (null == ID) {
      ID = request.getPathInfo();
    }

    /*DataStore ds = null;
         try {
      ds = new DataStore(Settings.get("vo_store.fileStoreDir"));
         }
         catch (DataStoreException ex1) {
      throw new ServletException(ex1);
         }*/

    //////////////
    User voUser = (User)request.getSession().getAttribute("voUser");
    String theAuthor = "";
    String coAuthor = "";
    try
    {
      String queryString = "xquery version \"1.0\"; " +
          "<results>" +
          "<author>{document('" + forumDB + "/" + ID +
          ".xml')/DISCUSSION/AUTHOR_ID/text()}</author>" +
          "<coauthor>{document('" + forumDB + "/" + ID +".xml')/DISCUSSION/CO_AUTHORS/USER/ID[text()='" +voUser.getProfileName() + "']/text()}</coauthor>" +
          "</results>";
      XQueryService discussService = (XQueryService) CollectionsManager.
          getService(forumDB, true, "XQueryService");
      ResourceSet result = discussService.query(queryString);
      if (result.getSize() > 0) {
        XMLResource resource = (XMLResource) result.getResource(0);
        Element resElm = (new DOMBuilder()).build( (org.w3c.dom.Document) (
            resource.getContentAsDOM())).getRootElement();
        theAuthor = resElm.getChildText("author");
        coAuthor = resElm.getChildText("coauthor");
      }
      else {
        log.error("Error getting data document path from discussion document: returned no results.");
        throw new IOException("Error getting data document path from discussion document: returned no results. " +
                            forumDB + "/" + ID);
      }
    }
    catch(XMLDBException e)
    {
      log.error("Error getting metadata.");
        throw new IOException("Error getting metadata. " +
                            forumDB + "/" + ID);

    }
    ///////////////
    if (!voUser.getProfileName().equals(theAuthor) && !voUser.getProfileName().equals(coAuthor)){
      return;
    }
    ///////////////

    String storageURL = null;
    String hostAddress = null;
    String authToken = null;

    Connection con = null;
    Statement stmt = null;
    try {
      con = ConnectionPool.getConnection("users");
      stmt = con.createStatement();

      Basket basket = (Basket) vobs.webapp.AdminAction.findBasket(stmt, theAuthor);
      storageURL = (String) basket.get("swiftStorageURL");
      hostAddress = "144.206.106.141";
      String authTokenExpired = (String) basket.get("swiftTokenExpired");
      long curTime = System.currentTimeMillis();
      if (Long.valueOf(authTokenExpired) < curTime) {
        /////
        String swiftLogin = (String) basket.get("swiftLogin");
        String swiftPassword = (String) basket.get("swiftPassword");
        authToken = SwiftDataStore.getToken(hostAddress, swiftLogin,
                                            swiftPassword);
        basket.put("swiftToken", authToken);
        basket.put("swiftTokenExpired", "" + (System.currentTimeMillis() + 82800000));

        vobs.webapp.AdminAction.storeBasket(stmt, basket, theAuthor);
        //System.out.println("swiftTokenExpired for user-" + theAuthor +" token-"+authToken);
        /////
      }
      else {
        authToken = (String) basket.get("swiftToken");
        //System.out.println("BASKET user-" + theAuthor +" token-"+authToken);
      }
    }
    catch (Exception e) {
      System.out.println("error! SwiftStoreServlet");
      e.printStackTrace();
    }

    ///////////////

    //System.out.println("storageURL='"+storageURL+"'");
    //System.out.println("ID='"+ID+"'");
    //System.out.println("hostAddress='"+hostAddress+"'");
    //System.out.println("authToken='"+authToken+"'");
    SwiftDataStore ds = new SwiftDataStore(storageURL, ID, hostAddress, authToken);
    //ds.swiftInit();
    if (null == ID || ID.length() == 0)
      return;

    InputStream inp = null;
    Element metaElm = null;
    //try {
      //inp = ds.getData(ID);
      inp = ds.getData();
      if(inp == null)
      {
        System.out.println("swift data input stream is null");
      }
    /*}
    catch (DataStoreException ex) {
      response.setContentType("text/html;charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(
          "<html><head><title>Error loading the file.</title></head><body>");
      out.println("<h3>Error! The file is not available from this server.</h3>");
      out.println("<p>You tried to access the file \"" + ID + "\", which is currently unavailable. Please make sure that the ID is correctly specified, then try reloading the page.</p>");
      out.println("</body></html>");
      return;
    }*/

    response.setContentType("text/xml;charset=UTF-8");

    //try {
      //metaElm = ds.getMetadata(ID);
      metaElm = ds.getMetadata();
    //}
    /*catch (DataStoreException ex) {
      PrintWriter out = response.getWriter();
      out.println(
          "<h3>Error! The metadata file is not available from this server.</h3>");
      out.println("<p>You tried to access the metadata file \"" + ID + ".xml\", which is currently unavailable. Please make sure that the ID is correctly specified, then try reloading the page.</p>");
      return;
    }*/

    OutputStream out = response.getOutputStream();
    String fileName = ID;
    String contentType = "application/octet-stream";

    if (null != metaElm) {
      fileName = metaElm.getChildText("fileName");
      contentType = metaElm.getChildText("contentType");
      response.setHeader("Content-Length", metaElm.getChildText("fileSize"));
    }

    response.setContentType(contentType);
    response.setHeader("Content-Disposition",
                       "inline; filename=\"" + fileName + "\"");

    int bufLen = 1024;

    if (null != request.getHeader("Range")) {
      response.setStatus(response.SC_PARTIAL_CONTENT);
      StringTokenizer tok = new StringTokenizer(request.getHeader("Range").
                                                replaceAll("bytes=", ""), "-");
      int start = Integer.parseInt(tok.nextToken()),
          end = Integer.parseInt(tok.nextToken()) + 1;
      response.setHeader("Content-Length", end - start + "");

      byte[] buffer = new byte[bufLen];
      int position = start;
      inp.skip(start);
      int bytesRead = inp.read(buffer, 0, bufLen);
      while (bytesRead > 0 && position < end) {
        out.write(buffer, 0, Math.min(end - position, bytesRead));
        bytesRead = inp.read(buffer, 0, bufLen);
        position += bytesRead;
      }
      inp.close();
      out.flush();
      //System.out.println("Position: "+position);

    }
    else {

      byte[] buffer = new byte[bufLen];
      int bytesRead = inp.read(buffer, 0, bufLen);
      while (bytesRead > 0) {
        out.write(buffer, 0, bytesRead);
        bytesRead = inp.read(buffer, 0, bufLen);
      }
      inp.close();
      out.flush();
    }
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) {
    PrintWriter writ = null;
    try {
      writ = response.getWriter();
    }
    catch (IOException ex1) {
      ex1.printStackTrace();
    }
    try {
      boolean isMultipart = ServletFileUpload.isMultipartContent(request);
      if (!isMultipart) {
        response.getWriter().write(
            "<h3>Request is not multipart, file upload cancelled</h3>");
        return;
      }

      DiskFileItemFactory factory = new DiskFileItemFactory(DiskFileItemFactory.
          DEFAULT_SIZE_THRESHOLD,
          new File(Settings.get("vo_store.fileStoreDir")));
      ServletFileUpload upload = new ServletFileUpload(factory);

      String fileName = null;
      String fileType = null;
      String ID = null;
      InputStream inp = null; ;

      Iterator iter = upload.parseRequest(request).iterator();
      while (iter.hasNext()) {
        FileItem item = (FileItem) iter.next();

        if (item.isFormField()) {
          if (item.getFieldName().equals("fileName")) {
            fileName = item.getString();
          }
          else if (item.getFieldName().equals("fileType")) {
            fileType = item.getString();
          }
          else if (item.getFieldName().equals("ID")) {
            ID = item.getString();
          }
        }
        else {
          inp = item.getInputStream();
        }
      }

      if (null != fileName && null != fileType && null != ID && null != inp) {
        FileStoreSave.storeFile(inp, fileName, fileType, inp.available() + "",
                                ID, "fileAction", null);
        inp.close();
        writ.write("<h3>File " + fileName +
                   " successfully stored. Thank you.</h3>");
      }
      else {
        writ.write(
            "<h3>Not found one of needed parameters in query string.</h3>");
        return;
      }

    }
    catch (FileUploadException ex) {
      ex.printStackTrace(writ);
      return;
    }
    catch (IOException ex) {
      ex.printStackTrace(writ);
      return;
    }
    catch (DataStoreException ex) {
      ex.printStackTrace(writ);
      return;
    }

  }

}
