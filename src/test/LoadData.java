package test;

import java.io.FileInputStream;
import java.util.Properties;

import ru.wdcb.activestorage.DataType;
import ru.wdcb.activestorage.Database;
import ru.wdcb.activestorage.Dimension;
import ru.wdcb.activestorage.Group;
import ru.wdcb.activestorage.Variable;

public class LoadData {
	public static void main(String[] args) throws Exception {
		
		Properties p = new Properties();
		p.load(new FileInputStream("config.properties"));
		String connectionString = p.getProperty("database.url");
		
		/*p.getProperty("identity.endpoint.publicURL");
		p.getProperty("auth.credentials");
		p.getProperty("auth.username");
		p.getProperty("auth.password");
		p.getProperty("auth.tenantName");*/
		
		Database db = new Database(connectionString);
		
		Group group = db.getRootGroup().createSubgroup("test");
		Dimension dimX = group.createDimension("x");
		Variable var = group.createVariable("var", new DataType(DataType.BYTE), new Dimension[] {dimX});
		//Variable var = group.getVariable("var");
		var.putData(new int[] {0}, new int[] {5}, new byte[] {0,1,2,3,4});
		//var.putData(new int[] {5}, new int[] {5}, new byte[] {5,6,7,8,9});
		//byte[] result = (byte[])var.getData(new int[] {0}, new int[] {5});
		/*
		for (int i=0; i< result.length; i++) {
			System.out.println(result[i]);
		}
		*/
	}
}
