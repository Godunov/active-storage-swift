package test;

import java.io.*;

import org.openstack.api.storage.AccountResource;
import org.openstack.client.OpenStackClient;
import org.openstack.model.storage.swift.SwiftStorageObjectProperties;
import java.util.Properties;

public class SwiftStoreSave {

  public static OpenStackClient openstack;
  public static AccountResource storage; 
  
  public static void storeFile(String fileName, String publicURL, String userName, String password, String tenantName) {
		Properties p = new Properties();
		p.put("identity.endpoint.publicURL", publicURL);
		p.put("auth.credentials", "passwordCredentials");
		p.put("auth.username", userName);
		p.put("auth.password", password);
		p.put("auth.tenantName", tenantName);
		
		
		
	  	openstack = OpenStackClient.authenticate(p); // Keystone authentication
	  	storage = openstack.getStorageEndpoint();    // Access to Swift Storage - endpoint
	  	
	  	// create container
	  	storage.container("VO").put();
	  	// create object
		storage.container("VO").object("dir1").put();
		
		// add file to object
		SwiftStorageObjectProperties prop = new SwiftStorageObjectProperties();
		storage.container("VO").object("dir1").put(new File(fileName), prop);
		
  }

  /** 
   * ��������� main() ������ ��� �������� ����������������� ����!!!
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
	  SwiftStoreSave.storeFile("d://NetCDF/air.sig995.2012.nc", 
			  				   "http://144.206.106.188:5000/v2.0", 
			  				   "maria", 
			  				   "333r-", 
			  				   "cloud");
  }

}
