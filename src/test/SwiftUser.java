package vobs.plugins;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import vobs.dbaccess.*;
import wdc.settings.Settings;
import org.apache.log4j.Logger;
/**
 * <p>Title: Virtual Observatory</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: CGDS</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SwiftUser {
  private static Logger log = Logger.getLogger(SwiftUser.class);
  //Section for init user in Swift system
  private static boolean Batch = false;
  private static String SWIFT_AUTH_ADM_KEY = "Change_ME";
  private static String[] usera = null;

  public static String[] swu_init(String username, String password) {
    //System.out.println("!!2!! kokovin---"+username+"---"+password);
    return swu_init("cloud1", username, password);
  }

  public static String[] swu_init(String account, String username,
                                   String password) {

    //System.out.println("!!3!! kokovin---"+username+"---"+password);
    try {
      log.debug("swu_init: looking for batch");
      File file = new File(Settings.get("vo_store.SwiftUserInitScript"));
      if (file.exists())
        Batch = true;
      log.debug("swu_init: Batch " + Batch);

    }
    catch (Exception e) {
      log.debug("Swift_User_Init fail");
      //e.printStackTrace();
      return null;
    }

    String Id = username + password.substring(0, 4); //.split("-")[0];

    String passwd = VOAccess.getUniqueId();

    usera = new String[] {account,Id,passwd,"",""};
    if (Batch) {
      try {
    	//  System.out.println("Script: "+"bash "+Settings.get("vo_store.SwiftUserInitScript")+" "+account+" "+Id+" "+passwd);	  
        Process p = Runtime.getRuntime().exec("bash "+Settings.get("vo_store.SwiftUserInitScript")+" "+account+" "+Id+" "+passwd);
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.
            getInputStream()));

        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.
            getErrorStream()));

        String s;
        boolean second_line = true;
        while ( (s = stdInput.readLine()) != null) {
          //System.out.println("Line 1: "+s.toString());	
          //get return code of process executing in second output line
          if (second_line && ( (s = stdInput.readLine()) != null)) {
            if (s.charAt(0) != '0') {
              log.debug("Swift_User_Init: swauth-add-user didn`t return success code");
              return null;
            }
            second_line = false;
          }
          if ( (s = stdInput.readLine()) != null) {
        	//  System.out.println("Line 2: "+s.toString());	  
            if (s.startsWith("https")) {
              //XStorageUrl = s;
              usera[3] = s;
              //XAuthToken = stdInput.readLine();
              usera[4] = stdInput.readLine();
            }
          }
        }

        String err;
        while ( (err = stdError.readLine()) != null) {
          log.debug("Swift_User_Init: ERROR: " + err);
          return null;
        }
        if (usera[3].equalsIgnoreCase("")) {
          log.debug("Swift_User_Init: fail to obtain XStorageURL ");
          return null;
        }
        return usera;
      }
      catch (Exception e) {
        log.error("Swift_User_Init fail", e);
        e.printStackTrace();
        return null;
      }
    }
    else { //do it via http. mb not secure!!!!
      System.out.println("Swift_User_Init useradd via http unsupported yet");
      return null;
    }
  }

  //end section swift user init

  public SwiftUser() {
  }

  public static void main(String[] args) {
    SwiftUser swiftuser = new SwiftUser();
  }
}
