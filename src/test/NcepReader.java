package test;

import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Properties;

import ru.wdcb.activestorage.Database;
import ru.wdcb.activestorage.Variable;

public class NcepReader {
	public void run() throws Exception {
	
		Properties p = new Properties();
		p.load(new FileInputStream("config.properties"));
		String connectionString = p.getProperty("database.url");
		
		Database db = new Database(connectionString);
		Variable var = db.getRootGroup().getSubgroup("surface").getVariable("air_sig995");
		long t1 = Calendar.getInstance().getTimeInMillis();
		short[] data = (short[])var.getData(new int[] {35075,0,0}, new int[] {1,73,144});
		long t2 = Calendar.getInstance().getTimeInMillis();
		System.out.print((t2-t1)/1000.0);
		
		/*for (int i=0; i<data.length; i++) {
			System.out.print(data[i]+"  ");
		}*/
		
		db.close();
	}
	
	public static void main(String[] args) throws Exception {
		NcepReader instance = new NcepReader();
		instance.run();
	}
}
