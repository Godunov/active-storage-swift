/*
SQLyog Community v9.20 
MySQL - 5.1.40-community : Database - activestorage
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`activestorage` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `activestorage`;

/*Table structure for table `dimensions` */

DROP TABLE IF EXISTS `dimensions`;

CREATE TABLE `dimensions` (
  `dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_group` int(11) DEFAULT NULL,
  `dim_name` varchar(255) DEFAULT NULL,
  `dim_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`dim_id`),
  KEY `FK_dimensions` (`dim_group`),
  CONSTRAINT `FK_dimensions` FOREIGN KEY (`dim_group`) REFERENCES `groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `dimensions` */

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `IX_groups` (`group_name`),
  KEY `FK_groups_groups` (`parent_id`),
  CONSTRAINT `FK_groups_groups` FOREIGN KEY (`parent_id`) REFERENCES `groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`group_id`,`group_name`,`parent_id`) values (1,'',NULL);

/*Table structure for table `grp_attributes` */

DROP TABLE IF EXISTS `grp_attributes`;

CREATE TABLE `grp_attributes` (
  `group_id` int(11) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `att_type` int(11) DEFAULT NULL,
  `att_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`att_name`),
  KEY `FK_grp_attributes_types` (`att_type`),
  CONSTRAINT `FK_grp_attributes_groups` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_grp_attributes_types` FOREIGN KEY (`att_type`) REFERENCES `types` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grp_attributes` */

/*Table structure for table `shapes` */

DROP TABLE IF EXISTS `shapes`;

CREATE TABLE `shapes` (
  `var_id` int(11) NOT NULL,
  `dim_index` int(11) NOT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `dim_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`var_id`,`dim_index`),
  KEY `FK_shapes_dimesions` (`dim_id`),
  CONSTRAINT `FK_shapes_dimesions` FOREIGN KEY (`dim_id`) REFERENCES `dimensions` (`dim_id`),
  CONSTRAINT `FK_shapes_variables` FOREIGN KEY (`var_id`) REFERENCES `variables` (`var_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shapes` */

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(10) DEFAULT NULL,
  `type_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `types` */

insert  into `types`(`type_id`,`type_name`,`type_length`) values (1,'char',1),(2,'byte',1),(3,'short',2),(4,'int',4),(5,'long',8),(6,'float',4),(7,'double',8),(8,'String',0);

/*Table structure for table `var_attributes` */

DROP TABLE IF EXISTS `var_attributes`;

CREATE TABLE `var_attributes` (
  `var_id` int(11) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `att_type` int(11) DEFAULT NULL,
  `att_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`var_id`,`att_name`),
  KEY `FK_var_attributes_types` (`att_type`),
  CONSTRAINT `FK_var_attributes_types` FOREIGN KEY (`att_type`) REFERENCES `types` (`type_id`),
  CONSTRAINT `FK_var_attributes_variables` FOREIGN KEY (`var_id`) REFERENCES `variables` (`var_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `var_attributes` */

/*Table structure for table `variables` */

DROP TABLE IF EXISTS `variables`;

CREATE TABLE `variables` (
  `var_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_group` int(11) DEFAULT NULL,
  `var_name` varchar(255) DEFAULT NULL,
  `var_type` int(11) DEFAULT NULL,
  `data_table` varchar(255) DEFAULT NULL,
  `index_table` varchar(255) DEFAULT NULL,
  `vector_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`var_id`),
  KEY `FK_variables_groups` (`var_group`),
  KEY `FK_variables_types` (`var_type`),
  CONSTRAINT `FK_variables_groups` FOREIGN KEY (`var_group`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_variables_types` FOREIGN KEY (`var_type`) REFERENCES `types` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `variables` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
